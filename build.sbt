name := "vts"

version := "1.0"

scalaVersion := "2.11.6"

//resolvers += "Local Maven Repository" at "file://"+Path.userHome.absolutePath+"/.m2/repository"
externalResolvers := Resolver.withDefaultResolvers(resolvers.value, mavenCentral = false)
libraryDependencies += "com.regblanc" %% "scala-smtlib" % "0.2"
libraryDependencies += "org.scala-lang.modules" % "scala-parser-combinators_2.11" % "1.0.4"
libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test"
libraryDependencies += "org.scalaz" %% "scalaz-core" % "7.1.3"
libraryDependencies += "com.googlecode.kiama" %% "kiama" % "1.8.0"
libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.12.5" % "test"
libraryDependencies += "org.parboiled" %% "parboiled" % "2.1.0"

libraryDependencies += "org.ow2.sat4j" % "org.ow2.sat4j.core" % "2.3.5"
libraryDependencies += "org.ow2.sat4j" % "org.ow2.sat4j.sat" % "2.3.5"

libraryDependencies += "com.github.scopt" %% "scopt" % "3.4.0"

libraryDependencies += "com.github.pathikrit" %% "better-files" % "2.15.0"

resolvers += Resolver.sonatypeRepo("public")
resolvers ++= Seq(
  Resolver.url("org.trupkin sbt plugins", url("http://dl.bintray.com/mtrupkin/sbt-plugins/"))(Resolver.ivyStylePatterns),
  "SpringSource" at "http://repository.springsource.com/maven/bundles/external"
)

scalacOptions ++= "-deprecation:true -feature".split("\\s+").to[Seq]

exportJars := true

test in assembly := {}
assemblyJarName in assembly := "vts.jar"
mainClass in assembly := Some("dk.itu.vts.Main")