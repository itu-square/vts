#!/bin/sh

projection='CONFIG_STATUS'
vts checkout -p $projection src/main/scala-2.11/
sbt assembly
Launch4j/launch4jc.exe launch4j-cfg.xml

vts checkin -a $projection src/main/scala-2.11/
