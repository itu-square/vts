package dk.itu.vts


import dk.itu.vts.commands.Command._

/**
  * Created by scas on 21-04-2016.
  */
object Main {

  import java.io.File

  case class Config(cmd: String = "", files: Seq[String] = Seq(), projection: String = "", ambition: String = "", addFile: Boolean = false)

  def main(args: Array[String]) {
    val currentDirectory = System.getProperty("user.dir").replace("\\", "/")

    def vtsInitialized(wd: String): Boolean = (new File(currentDirectory + "/.vts/")).exists

    val parser = new scopt.OptionParser[Config]("vts") {
      head("vts", "0.1")
      help("help") text ("prints this usage text")

      cmd("init") action { (_, c) =>
        c.copy(cmd = "init")
      }text("Initialize the system")

      cmd("add") action { (_, c) =>
        c.copy(cmd = "add")
      }text("Add a file to the system. The file will be moved from your folder to the VTS folder. Use VTS list to see which files are tracked") children (
          arg[String]("<file>...") unbounded() required() action { (x, c) =>
          c.copy(files = c.files :+ x)
        } text ("optional unbounded args")
      )

      cmd("list") action { (_, c) =>
        c.copy(cmd = "list")
      } text("Shows which files are added to the system")

      cmd("status") action { (_, c) =>
        c.copy(cmd = "status")
      } text("Shows which files are checked out and with what projection")
      cmd("checkout") action { (x, c) =>
        c.copy(cmd = "checkout")
      } text ("Checkout a file using a projection") children(
        opt[String]("projection") abbr ("p") optional() action { (x, c) =>
          c.copy(projection = x)
        },

        arg[String]("<file>...") unbounded() required() action { (x, c) =>
          c.copy(files = c.files :+ x)
        } text ("optional unbounded args")
        )

      cmd("checkin") action { (x, c) =>
        c.copy(cmd = "checkin")
      } text ("Checkin a file using the ambition") children(
        opt[String]("ambition") abbr ("a") optional() action { (x, c) =>
          c.copy(ambition =  x)
        },
        opt[String]("add") abbr ("add") optional() action { (x, c) =>
          c.copy(addFile = true)
        },
        arg[String]("<file>...") unbounded() required() action { (x, c) =>
          c.copy(files = c.files :+ x)
        } text ("optional unbounded args")
        )
    }

    parser.parse(args, Config()) match {
      case Some(config) => {
        if (!config.cmd.equalsIgnoreCase("init") && !(vtsInitialized(currentDirectory))) {
          println("VTS must be first initialized; use vts init command.")
        } else {
          config.cmd match {
            case "init" => Init(currentDirectory)
            case "add" => Add(currentDirectory,config.files.toList)
            case "list" => ListTrackedFiles(currentDirectory)
            case "checkout" => {
              config.projection match {
                case "" => {
                  Checkout(currentDirectory, "true", config.files)
                }
                case p => {
                  Checkout(currentDirectory, p, config.files)
                }
              }
            }
            case "checkin" => {
              config.ambition match {
                case a if a.isEmpty => Checkin(currentDirectory, "true", config.files.toList, config.addFile)
                case _ => Checkin(currentDirectory, config.ambition, config.files.toList, config.addFile)
              }
            }
            case "status" => Status(currentDirectory)
          }
        }
        }
        // do stuff

      case None =>
      // arguments are bad, error message will have been displayed
    }
  }
}