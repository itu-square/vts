package dk.itu.vts.commands

import java.nio.file.{Paths, Files}

import better.files._
import java.io.{File => JFile, BufferedReader, InputStreamReader}

import dk.itu.vts.expression.{ExpressionParser}
import dk.itu.vts.model.ProgamAST.Program
import dk.itu.vts.parser.Parser
import dk.itu.vts.util.{FileUtil, PrettyPrinter}
import scala.language.higherKinds

/**
  * Created by scas on 21-04-2016.
  */
object Command extends Storage with FileUtil{

  val UTF8 = java.nio.charset.StandardCharsets.UTF_8
  sealed abstract class Command(dir: String) {
    val vtsFolder = dir + "/" + VTSFolder
    val vtsSources = dir + "/" + VTSFolder + "/" + VTSSources
    val checkout = (vtsFolder + "/" + VTSCheckout).toFile
    val variantClone = vtsFolder + "/" + VTSClone
    val tracked = (vtsFolder + "/" + VTSList).toFile
  }

  case class Init(dir: String) extends Command(dir) {
    val f = vtsFolder.toFile
    f.exists match {
      case true => println("VTS was already initialized"); sys.exit(1);
      case false => {
        f.createDirectory
        checkout.createIfNotExists(false)
        tracked.createIfNotExists(false)
        vtsSources.toFile.createDirectory
      }
    }
    println("Variation tracking system initialized.")
  }

  /**
    * Adds a file to the variation tracking system
    * @param dir
    */
  case class Add(dir: String, files: List[String]) extends Command(dir) {
    // check if a file with the same name already exists
    val f = files.headOption match {
      case Some(x) => x match {
        case "." => filesRelativePath(".",true,FileTypes) map (x => x.replace("\\","/"))
        case f if Files.isDirectory(Paths.get(f)) && files.size == 1 => filesRelativePath("./"+f,true,FileTypes) map (x => x.replace("\\","/"))
        case f if !Files.isDirectory(Paths.get(f))=> files
      }
      case None => println("Exiting"); sys.exit(1);
    }

    val existingTrackedFiles = tracked.lines.toList
    val alreadyTrackedFiles = existingTrackedFiles.intersect(f)
    alreadyTrackedFiles.isEmpty match {
      case true =>  addFiles(f)
      case false => {
        println("You are trying to add files that are already tracked (or that have the same name). Only files that are not tracked will be added.")
        val newFiles = f.diff(existingTrackedFiles)
        addFiles(newFiles)
      }
    }

    private def addFiles(files: List[String]) = {
      files.foreach(x => {
        val dest = (vtsSources + "/" + x).toFile
        dest.parentOption match {
          case Some(x) => x.createDirectories()
          case None => Nil
        }
        try{
        (dir + "/" + x).toFile.copyTo(dest)
        (dir + "/" + x).toFile.delete(true)
        }
        catch {
          case ex: Exception => println("Some error occurred. Exiting");ex.printStackTrace(); sys.exit(1);
        }
      })
      tracked.appendLine(files.mkString("\n"))
    }
  }

  /**
    * Outputs to console a list of files that are tracked by the system.
    * @param dir
    */
  case class ListTrackedFiles(dir: String) extends Command(dir) {
      val trackedFiles = tracked.lines.toList
      trackedFiles.isEmpty match {
      case true => println("There are no files tracked")
      case false => trackedFiles.foreach(x => {println(x + " is tracked")})
    }
  }

  /**
    * Checkin command
    * @param dir
    * @param ambition
    * @param files
    */
  case class Checkin(dir: String, ambition: String, files:List[String], addFile: Boolean) extends Command(dir) {
    import scala.collection.JavaConversions._
    val data = checkout.lines.toList.map(x => {val split = x.split(";"); split(0) -> split(1)}).toMap
    val ambitionExpression = ExpressionParser.parse(ambition).getOrElse({println("Cannot parse the ambition"); sys.exit})
    val checkedoutFiles = data.keys.toList


    def checkin [T[_]](file: String) = {
      data.get(file) match {
        case Some(x) => {
          val parsedOrigAST = Parser.parseString(((vtsSources + "/" + file).toFile.lines.toList).mkString("\n")).right.get
          val parsedUpdatedAST = Parser.parseString((dir + "/" + file).toFile.lines.toList.mkString("\n")).right.get //Parser.parseFile(dir + "/" + file).right.get
          val projectionExpression = ExpressionParser.parse(data.get(file).get).getOrElse({println("Cannot parse the projection");sys.exit})
          dk.itu.vts.editing.Checkin.checkinWithNesting(projectionExpression,ambitionExpression, parsedOrigAST, parsedUpdatedAST)
        }
        case None if !tracked.lines.toList.contains(file) => {
          println("   " + file + " was not checked-out previously and it is not tracked by the system. It will be automatically tracked and added using the provided ambition")
          val parsedUpdatedAST = Parser.parseString((dir + "/" + file).toFile.lines.toList.mkString("\n")).right.get
          Add(dir, List(file))
          dk.itu.vts.editing.Checkin.checkinWithNesting(ambitionExpression,ambitionExpression, Nil, parsedUpdatedAST)
        }
        case None if tracked.lines.toList.contains(file) => {
          println("   " + file + " was not checked-out previously but it is tracked by the system. You have to first check out the file in order to check it in")
          sys.exit(1)
        }
      }
    }

    def  writeUpdate(file: String) = {
      val updatedSource = PrettyPrinter.prettyPrint(checkin(file))
      (vtsSources + "/" + file).toFile.write(updatedSource.mkString("\n"))
    }

    def pruneEmptyDirectories = findEmptyDirectories(vtsFolder).foreach(x => Files.delete(Paths.get(x)))

    val f = files.head match {
      case "." => checkedoutFiles
      case a if !(dir + "/" + a).toFile.exists => println("File " + a + " does not exist in this folder"); sys.exit(1);
      case a if Files.isDirectory(Paths.get(a)) => {

        // FIXME -- these should also work with folders
        if (!checkedoutFiles.contains(a)) // maybe adding a new file that is not checked out. This will be verified later
          checkedoutFiles.filter(x => x.startsWith(a)) ++ List(a)
        else
          checkedoutFiles.filter(x => x.startsWith(a))
      }
      case a if !Files.isDirectory(Paths.get(a)) => {
        if (!checkedoutFiles.contains(a)) // maybe adding a new file that is not checked out. This will be verified later
          checkedoutFiles.filter(x => x.equals(a)) ++ List(a)
        else
          checkedoutFiles.filter(x => x.equals(a))
      }
    }
    println("Check-in with ambition = " + ambition)
    f.foreach(x => {
      println("   " + x)
      writeUpdate(x)
    })

    val remainingUncheckedIn = checkedoutFiles.diff(checkedoutFiles.intersect(f))
    checkedoutFiles.isEmpty match{
      case true => Nil
      case false => {
        remainingUncheckedIn match {
          case List() => {
            checkout.clear
            println("All checked-out files were checked-in.")
          }
          case _ => {
            checkout.clear
            val uncheckedInFiles = data.--(f)
            uncheckedInFiles.keys.foreach(x => checkout << x + ";" + uncheckedInFiles.get(x).get)
            println("The following files were checked-out but not checked-in.")
            remainingUncheckedIn.foreach(y => println("  " + y ))
          }
        }
        // remove the temp files
        f.foreach(x => (dir + "/" + x).toFile.delete(true))
        pruneEmptyDirectories
      }
    }
  }


  /**
    * Checkout command
    * @param dir
    * @param projection
    * @param checkoutFiles
    */

  case class Checkout(dir: String, projection: String, checkoutFiles: Seq[String]) extends Command(dir) {


    val allFiles = checkoutFiles.head match {
      case "." => tracked.lines.toList //filesRelativePath(".",true,FileTypes) map (x => x.replace("\\","/"))
      case f if Files.isDirectory(Paths.get(f)) && checkoutFiles.size == 1 => {
        if (f.last.equals('/')) {
          tracked.lines.toList.filter(x => x.startsWith(f))
        }
        else {
          tracked.lines.toList.filter(x => x.startsWith(f + "/"))
        } //filesRelativePath(vtsSources + "/"+f,true,FileTypes) map (x => x.replace("\\","/"))
      }
      case f if !Files.isDirectory(Paths.get(f))=> checkoutFiles
    }

    val projectionExpression = ExpressionParser.parse(projection) match {
      case Some(x) => x
      case None => println("The projection is not a valid expression"); sys.exit(1)
    }

    private def makeProjection[T[_]](f: String) = {
      val parsedAST = Parser.parseFile(vtsSources + "/" + f)
      parsedAST.isRight match {
        case true => dk.itu.vts.editing.Checkout.partialProjection(projectionExpression,parsedAST.right.get)
        case false => sys.error(parsedAST.left.get)
      }
    }

    private def writeProjection[T[_]](projectedCode: Program[T], f: String) = {
      (dir + "/" + f).toFile.write(PrettyPrinter.prettyPrint(projectedCode).mkString("\n"))
    }

    def checkoutFile(file: String) = {
      if(checkout.lines.toList.map(x => x.split(";")(0)).contains(file)){
        println("File was already checked out. Are you sure you want to check it out again? (y/n) (Warning - all your current changes to the file will be lost)")
        scala.io.StdIn.readLine() match {
          case "y" =>  {
            val view = makeProjection(file)
            (dir + "/" + file).toFile.overwrite(PrettyPrinter.prettyPrint(view).mkString("\n"))
          }
          case "n" => Nil
        }
      }
      else{
        writeProjection(makeProjection(file),file)
      }
    }

    private def projectFiles(list: Seq[String]): Any = list match {
      case h +: t => {
        println("   " + h)
        checkoutFile(h)
        projectFiles(t)
      }
      case Seq(h) => {
        println("   " + h)
        checkoutFile(h)
      }
      case Seq() => Nil
    }

    println("Will check out files with projection = " + projection)
    println(allFiles)
    projectFiles(allFiles)
    allFiles.foreach(x => checkout << x + ";" + projection)

  }
  case class Status(dir: String) extends Command(dir) {
    val checkedOutFiles = checkout.lines.toList
    checkedOutFiles.isEmpty match {
      case true => println("No files are checked-out")
      case false => {
        println("The following files are checked-out")
        val f = checkedOutFiles.map(x => {val split = x.split(";"); split(0) -> split(1)}).toMap
        f.keys.toList.foreach(x => println("   " + x + " with projection: " + f.get(x).get))
      }
    }
  }
}