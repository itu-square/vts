package dk.itu.vts.commands

/**
  * Created by scas on 21-04-2016.
  */
trait Storage {

  val VTSFolder = ".vts"
  val VTSClone = "clones"
  val VTSFeatures = "features"
  val VTSCheckout = "checkout"
  val VTSProjection = "projection"
  val VTSList = "tracked"
  val VTSSources = "vts-srcs"

  val FileTypes = List[String]("c","cpp","h", "java","scala","ml","hs")

}