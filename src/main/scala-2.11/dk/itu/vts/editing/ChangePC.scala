package dk.itu.vts.editing

import dk.itu.vts.expression.Expression
import dk.itu.vts.model.ProgamAST.Program
import org.kiama.rewriting.{Rewriter, Strategy}

import scala.language.higherKinds
/**
  * Created by scas on 21-01-2016.
  */
object ChangePC extends Rewriter{

  def replacePCinAST[T[_]](previousPC: Expression, newPC: Expression, ast: Program[T]): Program[T] = {

    def replacePC[T[_]]: Strategy = everywhere {
      rule[Expression]{
        case x if x == previousPC => newPC
      }
    }
    rewrite(replacePC)(ast)
  }
}