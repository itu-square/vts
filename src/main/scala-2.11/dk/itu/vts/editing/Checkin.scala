package dk.itu.vts.editing

import dk.itu.vts.expression.{And, Expression, True}
import dk.itu.vts.model.ProgamAST._
import dk.itu.vts.model._
import dk.itu.vts.rewriter.SourceRules._
import dk.itu.vts.util.{PrettyPrinter, SatCheck}

import scala.language.higherKinds
/**
 * Created by scas on 02-11-2015.
 */
object Checkin extends SatCheck{

  /* p = a && p
    diff (a && p, s, v') = D<diff(a' && p', s, v'), s>   if p = D.l
                         = D<s , diff(a' && p', s, v')>  if p = D.r
                           v'                             p = empty
   */
  def checkin[T[_]](projection: Expression, ambition: Expression, origSource: Program[T], updatedSource: Program[T]): Program[T] = {
    if(!checkSat(projection, ambition))
      sys.error("Your fullProjection and ambition are contradicting - unsatisfiable. Please make the necessary changes")

    def update(expr: Expression, orSource: Program[T], upSource: Program[T]) = {
      YourChoice(expr, upSource, Some(orSource))
    }
    if(projection == ambition)
        rewriteAST(Seq(update(projection, origSource, updatedSource)))
    else
        rewriteAST(Seq(update(And(projection, ambition), origSource, updatedSource)))
  }

  def checkinWithNesting[T[_]](projection: Expression, ambition: Expression, origSource: Program[T], updatedSource: Program[T]): Program[T] = {
    def update(expr: Expression, orSource: Program[T], upSource: Program[T]) = {
      YourChoice(expr, upSource, Some(orSource))
    }

    def makeChoice(input: List[Expression], updatedSource: Program[T], originalSource: Program[T]): Program[T] = input match{
      case h :: List() => Seq(YourChoice(h, updatedSource, Some(originalSource)))
      case List(h: Expression) => Seq(YourChoice(h, updatedSource, Some(originalSource)))
      case List() => Nil
      case h :: t =>  Seq(YourChoice(h,makeChoice(t,updatedSource,origSource),Some(originalSource)))
    }
    if(projection == ambition){
      val result = makeChoice(projection.splitConjunctions,updatedSource,origSource)
      rewriteAST(result)
    }
    else if(projection == True()){
      val ast = Seq(YourChoice(ambition,updatedSource, Some(origSource)))
      rewriteAST(ast)
    }
    else{
      val newChoice = Seq(YourChoice(ambition,updatedSource, Some(origSource)))
      val result = makeChoice(projection.splitConjunctions,newChoice,origSource)
      rewriteAST(result)
    }
  }
}