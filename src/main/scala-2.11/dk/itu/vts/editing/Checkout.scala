package dk.itu.vts.editing

import dk.itu.vts.expression._
import dk.itu.vts.model.ProgamAST._
import dk.itu.vts.model._
import dk.itu.vts.smt.SMT._
import dk.itu.vts.util.SatCheck
import org.kiama.rewriting.{Rewriter}

import scala.language.higherKinds
/**
 * Created by scas on 25-10-2015.
 */
object Checkout extends Rewriter {


  def fullProjection[T[_]](projection: Expression, ast: Program[T]) = {

    def preprocessNodes(input: Program[T]): Program[T] ={
      input match {
        case h +: t => h match {
          case YourChoice(expr: Expression, left: Program[T], right: Option[Program[T]]) if (checkSAT(expr, projection)) => preprocessNodes(left) ++ preprocessNodes(t)
          case YourChoice(expr: Expression, left: Program[T], Some(right: Program[T])) if (!checkSAT(expr, projection)) => preprocessNodes(right) ++ preprocessNodes(t)
          case YourChoice(expr: Expression, left: Program[T], None) if (!checkSAT(expr, projection)) => preprocessNodes(t)
          case NoChoice(x) => Seq(NoChoice(x)) ++ preprocessNodes(t)
          case s => Seq(s) ++ preprocessNodes(t)
        }
        case Seq(x) => x match {
          case YourChoice(expr: Expression, left: Program[T], right: Option[Program[T]]) if (checkSAT(expr, projection)) => preprocessNodes(left)
          //case YourChoice(expr: Expression, left: Program[T], right: Option[Program[T]]) if !(checkSAT(expr, projection)) => right match {case Some(x) => x; case None => Nil}
          case YourChoice(expr: Expression, left: Program[T], Some(right: Program[T])) if (!checkSAT(expr, projection)) => preprocessNodes(right)
          case YourChoice(expr: Expression, left: Program[T], None) if (!checkSAT(expr, projection)) => Nil
          case s => Seq(s)
        }
        case Seq() => Nil
      }
    }
      preprocessNodes(ast)
  }

  def partialProjection[T[_]](projection: Expression, ast: Program[T]) = {

    def preprocessNodes(input: Program[T]): Program[T] ={
      input match {
        case h +: t => h match {

          case YourChoice(expr: Expression, left: Program[T], Some(right: Program[T])) if !(checkSAT(expr, projection)) => preprocessNodes(right) ++ preprocessNodes(t)
          case YourChoice(expr: Expression, left: Program[T], None) if !(checkSAT(expr, projection)) => preprocessNodes(t)
          case YourChoice(expr: Expression, left: Program[T], Some(right: Program[T])) if (checkSAT(expr, projection) && checkSAT(Negation(expr).removeDoubleNegation, projection)) => Seq(YourChoice(expr, preprocessNodes(left), Some(preprocessNodes(right)))) ++ preprocessNodes(t)
          case YourChoice(expr: Expression, left: Program[T], right: Option[Program[T]]) if !(checkSAT(Negation(expr).removeDoubleNegation, projection)) => preprocessNodes(left) ++ preprocessNodes(t)
          case YourChoice(expr: Expression, left: Program[T], None) if (checkSAT(expr, projection)) => Seq(YourChoice(expr, preprocessNodes(left), None)) ++ preprocessNodes(t)
          case Seq() => Nil
          case s => Seq(s) ++ preprocessNodes(t)
        }
        case Seq(x) => x match {
          case YourChoice(expr: Expression, left: Program[T], right: Option[Program[T]]) if !(checkSAT(Negation(expr).removeDoubleNegation, projection)) =>  preprocessNodes(left)
          case YourChoice(expr: Expression, left: Program[T], Some(right: Program[T])) if !(checkSAT(expr, projection)) => preprocessNodes(right)
          case YourChoice(expr: Expression, left: Program[T], None) if !(checkSAT(expr, projection)) => Nil
          case YourChoice(expr: Expression, left: Program[T], Some(right: Program[T])) if (checkSAT(expr, projection) && checkSAT(Negation(expr).removeDoubleNegation, projection)) => Seq(YourChoice(expr, preprocessNodes(left), Some(preprocessNodes(right))))
          case YourChoice(expr: Expression, left: Program[T], None) if (checkSAT(expr, projection)) => Seq(YourChoice(expr, preprocessNodes(left), None))
          case Seq() => Nil
          case s => Seq(s)
        }
        case Seq() => Nil
      }
    }
    if(projection == True())
      ast
    else preprocessNodes(ast)
  }
}