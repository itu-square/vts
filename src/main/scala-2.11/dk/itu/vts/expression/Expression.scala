package dk.itu.vts.expression

import java.math.BigInteger

import org.kiama.rewriting.Rewriter._
import org.kiama.rewriting.{Rewriter, Strategy}

import scala.language.{implicitConversions, postfixOps}
object ExpressionConversions {
  /** An Internal DSL for constructing expressions */
  //implicit def string2Identifier(str : String) = Identifier(str)
  implicit def expression2expressionRewriter(expr : Expression) = new ExpressionRewriter(expr)
}
class ExpressionRewriter(expr : Expression) extends Rewriter {
  //TODO can I use the reduce strategy?

  val demorgansRule = reduce {
    rule [Expression]{
      case Negation(And(x,y)) => Or(Negation(x), Negation(y))
      case Negation(Or(x,y)) => And(Negation(x), Negation(y))
    }
  }

  val impliesRule = everywheretd {
    rule [Expression]{
      case Implies(x,y) => Or(Negation(x), y)
    }
  }

  val doubleNegationRule = reduce {
    rule [Expression] {
      case Negation(Negation(x)) => x
    }
  }

  val normalizeRule = reduce {
    rule [Expression]{
      case And(x, And(y,z)) => And(And(x,y),z)
      //case Or(x, Or(y,z)) => Or(Or(x,y),z)
    }
  }

  //Must use fixpoint function
  val cnfRule = everywheretd {
    rule [Expression]{
      case Or(x,And(y,z)) => And(Or(x,y), Or(x,z))
      case Or(And(x,y),z) => And(Or(x,z), Or(y,z))
    }
  }

  /** Compute a fixpoint using equality instead of strategy failure. */
  private def fixpoint(s : => Strategy)(e : Expression) : Expression = {
    def iter(last : Expression) : Expression = {
      val result = rewrite(s)(last)
      if (result == last) last else iter(result)
    }
    iter(e)
  }

  def reduceNegations = rewrite(doubleNegationRule)(expr)

  def reduceImplies = rewrite(impliesRule)(expr)

  def normalize = rewrite(normalizeRule)(expr)

  def removeDoubleNegations = rewrite(doubleNegationRule)(expr)

  def toCNF = {
    val preCNF = rewrite(impliesRule <* demorgansRule <* doubleNegationRule)(expr)
    val postCNF = fixpoint(cnfRule)(preCNF)
    //rewrite(normalizeRule)(postCNF)
    //fixpoint(normalizeRule)()
    postCNF
  }
}

trait Expr

object Expression {
  implicit def toExprList(lst: List[Expression]) = new ExprList(lst)

  class ExprList(lst: List[Expression]) {
    def ||(): Expression = ((False(): Expression) /: lst){ _ | _ }
  }
}

sealed abstract class Expression extends Expr{
  def & (other : Expression) : Expression = other match {
    case True() => this
    case _ => And(this, other)
  }
  def | (other : Expression) : Expression = other match {
    case False() => this
    case _ => Or(this, other)
  }
  def unary_! = Negation(this)

  def implies (other : Expression) : Expression = Implies(this, other)

  def identifiers: Set[String] = collects {
    case True() => "true"
    case False() => "false"
      case Identifier(v) => v
      case Defined(v) => v
      case IntLiteral(v) => v.toString
      case CharacterLiteral(v) => v.toString
      case DecimalLiteral(v) => v.toString
      case FloatLiteral(v) => v.toString
      case HexLiteral(v) => v
      case OctalLiteral(v) => v
      case StringLiteral(v) => v
    }(Expression.this)

  def identifiersForMetrics: Set[String] = collects {
    //case Identifier(v) => v
    case Defined(v) => v
  }(Expression.this)

  def identifiersCheckout: Set[String] = collects {
    case Identifier(v) => v
    case Defined(v) => v
  }(Expression.this)

  def splitConjunctions(): List[Expression] = this match {
    case And(x,y) => x.splitConjunctions ::: y.splitConjunctions
    case e => List(e)
  }
  def splitDisjunctions(): List[Expression] = this match {
    case Or(x,y) => x.splitDisjunctions ::: y.splitDisjunctions
    case e => List(e)
  }

  lazy val simplify: Expression = this

  lazy val removeDoubleNegation: Expression = new ExpressionRewriter(this).removeDoubleNegations

  def toCNF = new ExpressionRewriter(this) toCNF

}

sealed abstract class BinaryOp(val left : Expression, val right : Expression, sym : String) extends Expression {
  override def toString = "(" + left + " " + sym + " " + right + ")"
}

sealed abstract class UnaryOp(val expr : Expression, sym : String) extends Expression {
  override def toString = sym + expr
}


case class And(l : Expression, r : Expression) extends BinaryOp(l, r, "&&")
case class Or(l : Expression, r : Expression) extends BinaryOp(l, r, "||")
case class TempBinaryOp(l : Expression , r : Expression) extends BinaryOp(l, r, "[TEMP]")
case class Implies(l : Expression, r : Expression) extends BinaryOp(l, r, "=>")

case class QuestionMark(expr : Expression, one : Expression, zero : Expression) extends Expression {
  override def toString = "(" + expr + " ? " + one + " : " + zero + ")"
}

case class QMark(one: Expression, zero:Expression) extends Expression {
  override def toString = one + " : " + zero
}

////Not present in Preprocessor
//case class FragmentIdentifier(num : Int) extends Expression {
//  override def toString = "F" + num
//}
//case class ErrorLiteral(str : String) extends Expression

case class Identifier(name : String) extends Expression {
  override def toString = name
}
case class IntLiteral(value : BigInteger) extends Expression {
  override def toString = value toString
}
object IntLiteral {
  def apply(value : Int) = new IntLiteral(BigInteger.valueOf(value))
}

case class StringLiteral(value : String) extends Expression {
  override def toString = value toString
}
case class FloatLiteral(value : String) extends Expression {
  override def toString = value toString
}
case class CharacterLiteral(value: String) extends Expression {
  override  def toString = value toString
}
case class HexLiteral(value: String) extends Expression {
  override  def toString =  value toString
}
case class OctalLiteral(value: String) extends Expression{
  override def toString = value toString
}
case class DecimalLiteral(value: String) extends Expression{
  override def toString = value toString
}
case class Defined(name : String) extends Expression {
  override def toString = "defined(" + name + ")"
}
case class DefinedValue(name : String, value: Int) extends Expression {
  override def toString = "defined(" + name + " " + value + ")"
}
case class Negation(expr : Expression) extends Expression {
  override def toString = "!" + expr
}
case class TempUnaryOp(expr : Expression) extends Expression

case class BitwiseAnd(l : Expression , r : Expression) extends BinaryOp(l, r, "&")
case class BitwiseOr(l : Expression , r : Expression) extends BinaryOp(l, r, "|")
case class BitwiseXor(l : Expression , r : Expression) extends BinaryOp(l, r, "^")
case class BitwiseNegation(expr : Expression) extends Expression {
  override def toString = "~" + expr
}

case class LessThan(l : Expression , r : Expression) extends BinaryOp(l, r, "<")
case class LessThanOrEquals(l : Expression , r : Expression) extends BinaryOp(l, r, "<=")
case class GreaterThan(l : Expression , r : Expression) extends BinaryOp(l, r, ">")
case class GreaterThanOrEquals(l : Expression , r : Expression) extends BinaryOp(l, r, ">=")
case class EqualsTo(l : Expression, r : Expression) extends BinaryOp(l, r, "==")
case class NotEqualsTo(l : Expression, r : Expression) extends BinaryOp(l, r, "!=")

case class Times(l : Expression, r : Expression) extends BinaryOp(l, r, "*")
case class Divide(l : Expression, r : Expression) extends BinaryOp(l, r, "/")
case class Plus(l : Expression, r : Expression) extends BinaryOp(l, r, "+")
case class Minus(l : Expression, r : Expression) extends BinaryOp(l, r, "-")
case class Mod(l : Expression, r : Expression) extends BinaryOp(l, r, "%")

case class ShiftLeft(l : Expression, r : Expression) extends BinaryOp(l, r, "<<")
case class ShiftRight(l : Expression, r : Expression) extends BinaryOp(l, r, ">>")

case class PrePlusPlus(expr : Expression) extends Expression
case class PreMinusMinus(expr : Expression) extends Expression
case class PostPlusPlus(expr : Expression) extends Expression
case class PostMinusMinus(expr : Expression) extends Expression
case class UnaryMinus(e : Expression) extends UnaryOp(e, "-")
case class UnaryPlus(e : Expression) extends UnaryOp(e, "+")
case class SizeOf(e : Expression) extends UnaryOp(e, "sizeof:")

case class True() extends Expression {
  override def & (other: Expression) = other
  override def implies (other: Expression) = other
  override def toString = "1"
}
case class False() extends Expression {
  override def | (other : Expression) = other
  override def toString = "0"
}

case class BIff(l: Expression, r: Expression) extends Expression {
  override def toString = "(" + l + " <=> " + r + ")"
}
case class FunctionCall(id: String, params : List[Expression]) extends Expression {

  def paramsString(p: List[Expression]): String = {
    val result = new StringBuilder
    for (i <- p) {result.append(i + ",")}
    result.toString.dropRight(1)
  }
  override def toString = id + "(" + paramsString(params)  + ")"
}