package dk.itu.vts.expression

import dk.itu.vts.rewriter.ExpressionRules._

import scala.util.parsing.combinator._

object ExpressionParserBoolean extends RegexParsers {

  lazy val expr : Parser[Expression] = orExpr

//    lazy val questionMarkExpr: Parser[Expression] = orExpr ~ opt("?" ~> qmark) ^^ {
//      case x ~ Some(y) => QuestionMark(x, y.one, y.zero)
//      case x ~ None => x
//    }
//    lazy val qmark: Parser[QMark] = expr ~ ":" ~ expr ^^{
//      case x ~ y => QMark(x._1,y)
//    }

  lazy val orExpr : Parser[Expression] = andExpr ~ opt("||" ~> orExpr) ^^ {
    case x ~ Some(y) => Or(x,y)
    case x ~ None => x
  }

  lazy val andExpr : Parser[Expression] = unaryExpr ~ opt("&&" ~> andExpr) ^^ {
    case x ~ Some(y) => And(x,y)
    case x ~ None => x
  }
//
//  lazy val bitwiseOr: Parser[Expression] = bitwiseXor ~ opt("|" ~> bitwiseOr) ^^ {
//    case x ~ Some(y) => BitwiseOr(x,y)
//    case x ~ None => x
//  }
//  lazy val bitwiseXor: Parser[Expression] = bitwiseAnd ~ opt("^" ~> bitwiseXor) ^^ {
//    case x ~ Some(y) => BitwiseXor(x,y)
//    case x ~ None => x
//  }
//
//  lazy val bitwiseAnd: Parser[Expression] = equalsToExpr ~ opt("&" ~> bitwiseAnd) ^^ {
//    case x ~ Some(y) => BitwiseAnd(x,y)
//    case x ~ None => x
//  }
//
//  lazy val equalsToExpr : Parser[Expression] = notEqualsToExpr ~ opt(("==" | "=") ~> equalsToExpr) ^^ {
//    case x ~ Some(y) => EqualsTo(x,y)
//    case x ~ None => x
//  }
//  lazy val notEqualsToExpr : Parser[Expression] = greaterThanExpr ~ opt("!=" ~> notEqualsToExpr) ^^ {
//    case x ~ Some(y) => NotEqualsTo(x,y)
//    case x ~ None => x
//  }
////  lazy val implExpr : Parser[Expression] = greaterThanExpr ~ opt("=>" ~> implExpr) ^^ {
////    case x ~ Some(y) => Implies(x,y)
////    case x ~ None => x
////  }
//  lazy val greaterThanExpr: Parser[Expression] = greaterOrEqualsThanExpr ~ opt(">" ~> greaterThanExpr) ^^ {
//    case x ~ Some(y) => GreaterThan(x, y)
//    case x ~ None => x
//  }
//  lazy val greaterOrEqualsThanExpr: Parser[Expression] = lessThanExpr ~ opt(">=" ~> greaterOrEqualsThanExpr) ^^ {
//    case x ~ Some(y) => GreaterThanOrEquals(x, y)
//    case x ~ None => x
//  }
//  lazy val lessThanExpr: Parser[Expression] = lessThanOrEqualsExpr ~ opt("<" ~> lessThanExpr) ^^ {
//    case x ~ Some(y) => LessThan(x, y)
//    case x ~ None => x
//  }
//  lazy val lessThanOrEqualsExpr: Parser[Expression] = shiftLeft ~ opt("<=" ~> lessThanOrEqualsExpr) ^^ {
//    case x ~ Some(y) => LessThanOrEquals(x, y)
//    case x ~ None => x
//  }
//  lazy val shiftLeft: Parser[Expression] = shiftRight ~ opt("<<" ~> shiftLeft) ^^ {
//    case x ~ Some(y) => ShiftLeft(x,y)
//    case x ~ None => x
//  }
//  lazy val shiftRight: Parser[Expression] = plus ~ opt(">>" ~> shiftRight) ^^ {
//    case x ~ Some(y) => ShiftRight(x,y)
//    case x ~ None => x
//  }
//
//  lazy val plus: Parser[Expression] = minus ~ opt("+" ~> plus) ^^ {
//    case x ~ Some(y) => Plus(x,y)
//    case x ~ None => x
//  }
//
//  lazy val minus: Parser[Expression] = times ~ opt("-" ~> minus) ^^ {
//    case x ~ Some(y) => Minus(x,y)
//    case x ~ None => x
//  }
//
//  lazy val times: Parser[Expression] = divide ~ opt("*" ~> times) ^^ {
//    case x ~ Some(y) => Times(x,y)
//    case x ~ None => x
//  }
//  lazy val divide: Parser[Expression] = mod ~ opt("/" ~> divide) ^^ {
//    case x ~ Some(y) => Divide(x,y)
//    case x ~ None => x
//  }
//
//  lazy val mod: Parser[Expression] = unaryExpr ~ opt("%" ~> mod) ^^ {
//    case x ~ Some(y) => Mod(x,y)
//    case x ~ None => x
//  }

  lazy val trueExpr : Parser[Expression] = "1".r ^^ (x => True())
  lazy val falseExpr: Parser[Expression] = "0".r ^^ (x => False())
  // FIXME unary minus can also take floats e.g. -1.2
//  lazy val unaryMinus: Parser[Expression] = "-" ~> """\d+""".r ^^ (x => UnaryMinus(IntLiteral(Integer.valueOf(x))))
//  lazy val intLiteral: Parser[Expression] = """\d+""".r ^^ (x => (IntLiteral(new BigInteger(x))))
//  lazy val floatLiteral: Parser[Expression] = """(\d+\.\d+)(f?)""".r ^^ FloatLiteral
//  lazy val characterLiteral: Parser[Expression] = """'(\w|\S)'""".r ^^ CharacterLiteral
//  lazy val hexLiteral: Parser[Expression] = """\dx(\w|\d)+""".r ^^ HexLiteral
//  lazy val stringLiteral: Parser[Expression] = """"(\w|\S)+"""".r ^^ StringLiteral
//  lazy val octalLiteral: Parser[Expression] = """(?i)\d+(ul|u|l)""".r ^^ OctalLiteral
  lazy val defFix: Parser[Expression] = opt("""\s*\(""".r) ~> """\s*\w+\s*""".r <~ opt(")") ^^ Defined
  lazy val defExpr: Parser[Expression] = "defined" ~ opt("""\s*\(""".r) ~> """\s*\w+\s*""".r <~ opt(")") ^^ Defined
  lazy val defExprFix: Parser[Expression] = "defined" ~> """\s*\w+\s*""".r ^^ Defined
  lazy val notDefExpr: Parser[Expression] = "not" ~ opt("""\s*""".r) ~ "defined" ~ opt("""\s*\(""".r) ~> """\s*\w+\s*""".r <~ opt(")") ^^ (x => Negation(Defined(x)))
//  lazy val functionCall: Parser[Expression] = """\w+""".r ~ "(" ~ (primary <~ opt("," <~ opt(" ".r))).*  ~ ")" ^^ (x => FunctionCall(x._1._1._1, x._1._2))
  lazy val enabled: Parser[Expression] = "ENABLED" ~ opt("""\s*\(\s*""".r) ~> primary <~ opt("""\s*\)\s*""".r)
//  lazy val disabled: Parser[Expression] = "DISABLED" ~ opt("""\s*\(\s*""".r) ~> primary <~ opt("""\s*\)\s*""".r)  ^^ Negation

  lazy val unaryExpr : Parser[Expression] =  ("!" ~> unaryExpr) ^^ Negation | primary
  lazy val primary : Parser[Expression] =  ifndef | ifdef | ifPrimary | elIfPrimary | enabled | "(" ~> expr <~ ")" |notDefExpr| defExprFix | defExpr |  trueExpr | falseExpr |   """\w+""".r ^^ Identifier
  lazy val primary1 : Parser[Expression] = ("!" ~> primary1) ^^ Negation  | enabled | defFix | "(" ~> expr <~ ")" | defExprFix |  notDefExpr | defExpr | trueExpr | falseExpr | """\w+""".r ^^ Defined

  lazy val ifdef: Parser[Expression] = """\s*\#\s*ifdef\s*""".r ~> primary1
  lazy val ifndef: Parser[Expression] = """\s*\#\s*ifndef\s*""".r ~> primary1 ^^ Negation
  lazy val ifPrimary: Parser[Expression] = """\s*\#\s*if\s*""".r ~> unaryExpr
  lazy val elIfPrimary: Parser[Expression] = """\s*\#\s*elif\s*""".r ~> unaryExpr


  def parse(str : String): Option[Expression] ={
    val strToBeParsed = str.split("//")(0)
    val finalString = strToBeParsed.replaceAll("/\\*.*", "").replace(";", "")
    //println(finalString.replaceAll("(\\/\\*)(.*)", ""))
    //println(finalString)
    // hack to remove # from busybox file #cpu(sparc)
    //println(str)
    parseAll(expr, finalString)
    match {
      case Success(matched, _) => Some(org.kiama.rewriting.Rewriter.rewrite(andOrExp)(matched.removeDoubleNegation))
      //case _ => {println("Cannot parse expression: " + str); None}
      case Failure(msg, _) => println("FAILURE: " + msg); None
      case Error(error, _) => println("ERROR: " + error); None
    }
  }

}