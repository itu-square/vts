package dk.itu.vts.model

import dk.itu.vts.expression.Expression

import scala.language.{higherKinds, implicitConversions}

/**
 * Created by scas on 27-08-2015.
 */

/**
 * The choice calculus representation. Choiced is a type constructor so that we can use any kind of ASTs
 * @tparam T
 */

sealed abstract class Choiced[T[_]]

case class NoChoice[T[_]](t: T[Choiced[T]]) extends Choiced[T] {
  override def toString = t.toString
}

case class YourChoice[T[_]](expr: Expression, left: Seq[Choiced[T]], right: Option[Seq[Choiced[T]]]) extends Choiced[T]