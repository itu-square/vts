package dk.itu.vts.model

/**
 * Created by scas on 14-10-2015.
 */
class TextlineModel {type Program = Seq[Choiced[Textline]]}

case class Textline[E](text: String) {
  override def toString = text.toString
}