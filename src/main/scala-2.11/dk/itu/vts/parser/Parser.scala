package dk.itu.vts.parser

import java.nio.charset.MalformedInputException

import dk.itu.vts.model.{Choiced, NoChoice, Textline, YourChoice}
import dk.itu.vts.util.FileNormalizer
import org.kiama.rewriting.{Rewriter, Strategy}
import org.parboiled2.{ErrorFormatter, ParseError}

import scala.language.higherKinds
import scala.util.{Failure, Success}
/**
 * Created by scas on 14-10-2015.
 */
object Parser extends Rewriter{

  type TextlineProgram = Seq[Choiced[Textline]]
  /*
    Could not figure how to build the dk.itu.vts.parser without adding empty lines. Thus, this removes blank empty lines that do not exist in the original file
   */
  //FIXME this needs to be refactored... or the dk.itu.vts.parser fixed to not introduce empty new lines after else.
  def removeEmptyChoiceInElseBranch [T[_]]: Strategy = everywherebu {
    rule[YourChoice[T]]{
      case YourChoice(expr, left, Some(x)) if x.head == NoChoice[Textline](Textline[Choiced[Textline]]("")) => YourChoice(expr, left, Some(x.tail))
    }
  }

  /**
   * Parse a file given its path
   * @param file
   * @return
   */

  def parseFile(file: String): Either[String, TextlineProgram] = {
    val stdEncoding = "UTF-8"

    try {
      TextlineParser.parseString(FileNormalizer.normalizeFile(file, stdEncoding)) match {
      case Success(e) => {
        val result = rewrite(removeEmptyChoiceInElseBranch)(e)
        Right(result)
      }
      case Failure(e: ParseError) => {
        // try again with different encoding
        try {
          TextlineParser.parseString(FileNormalizer.normalizeFile(file, "ISO-8859-2")) match {
            case Success(e) => {
              val result = rewrite(removeEmptyChoiceInElseBranch)(e)
              Right(result)
            }
            case Failure(e: ParseError) => {
              Left(e.format("", new ErrorFormatter(showTraces = true))) //println(e.format("", new ErrorFormatter(showTraces = true))); sys.error(e.format("", new ErrorFormatter(showTraces = true)))
            }
            case Failure(e) => {
              //println("Unexpected error during parsing: " + e);
              Left(e.getMessage) //println(e.format("", new ErrorFormatter(showTraces = true))); sys.error(e.format("", new ErrorFormatter(showTraces = true)))
            }
          }

        }
        catch {
          case e: MalformedInputException => Left("cannot parse this file neither with ISO-8859-2 nor with UTF-8")
          case e: Exception => Left("cannot parse this file neither with ISO-8859-2 nor with UTF-8; got weird exception: " + e.getMessage)
        }

      }
      case Failure(e) => {
        //println("Unexpected error during parsing: " + e);
        Left(e.getMessage) //println(e.format("", new ErrorFormatter(showTraces = true))); sys.error(e.format("", new ErrorFormatter(showTraces = true)))
      }
    }
    } catch {
      case x: MalformedInputException => TextlineParser.parseString(FileNormalizer.normalizeFile(file, "ISO-8859-2")) match {
        case Success(e) => {
          val result = rewrite(removeEmptyChoiceInElseBranch)(e)
          Right(result)
        }
        case Failure(e: ParseError) => {
          Left(e.format("", new ErrorFormatter(showTraces = true))) //println(e.format("", new ErrorFormatter(showTraces = true))); sys.error(e.format("", new ErrorFormatter(showTraces = true)))
        }
        case Failure(e) => {
          //println("Unexpected error during parsing: " + e);
          Left(e.getMessage) //println(e.format("", new ErrorFormatter(showTraces = true))); sys.error(e.format("", new ErrorFormatter(showTraces = true)))
        }
      }
    }

  }

  /**
   * Parse a string
   * @param src
   * @return
   */
  def parseString(src: String): Either[String, TextlineProgram] = {
    try {
      TextlineParser.parseString(FileNormalizer.normalizeFileFromString(src)) match {
        case Success(e) => {
          val result = rewrite(removeEmptyChoiceInElseBranch)(e)
          Right(result)
        }
        case Failure(e: ParseError) => {
          // try again with different encoding
          try {
            TextlineParser.parseString(FileNormalizer.normalizeFileFromString(src)) match {
              case Success(e) => {
                val result = rewrite(removeEmptyChoiceInElseBranch)(e)
                Right(result)
              }
              case Failure(e: ParseError) => {
                Left(e.format("", new ErrorFormatter(showTraces = true))) //println(e.format("", new ErrorFormatter(showTraces = true))); sys.error(e.format("", new ErrorFormatter(showTraces = true)))
              }
              case Failure(e) => {
                //println("Unexpected error during parsing: " + e);
                Left(e.getMessage) //println(e.format("", new ErrorFormatter(showTraces = true))); sys.error(e.format("", new ErrorFormatter(showTraces = true)))
              }
            }

          }
          catch {
            case e: MalformedInputException => Left("cannot parse this file neither with ISO-8859-2 nor with UTF-8")
            case e: Exception => Left("cannot parse this file neither with ISO-8859-2 nor with UTF-8; got weird exception: " + e.getMessage)
          }

        }
        case Failure(e) => {
          //println("Unexpected error during parsing: " + e);
          Left(e.getMessage) //println(e.format("", new ErrorFormatter(showTraces = true))); sys.error(e.format("", new ErrorFormatter(showTraces = true)))
        }
      }
    } catch {
      case x: MalformedInputException => TextlineParser.parseString(FileNormalizer.normalizeFileFromString(src)) match {
        case Success(e) => {
          val result = rewrite(removeEmptyChoiceInElseBranch)(e)
          Right(result)
        }
        case Failure(e: ParseError) => {
          Left(e.format("", new ErrorFormatter(showTraces = true))) //println(e.format("", new ErrorFormatter(showTraces = true))); sys.error(e.format("", new ErrorFormatter(showTraces = true)))
        }
        case Failure(e) => {
          //println("Unexpected error during parsing: " + e);
          Left(e.getMessage) //println(e.format("", new ErrorFormatter(showTraces = true))); sys.error(e.format("", new ErrorFormatter(showTraces = true)))
        }
      }
    }
//    TextlineParser.parseString(FileNormalizer.normalizeFileFromString(src)) match {
//      case Success(e) =>{
//        val result = rewrite(removeEmptyChoiceInElseBranch)(e)
//        Right(result)
//      }
//      case Failure(e: ParseError) => sys.error(e.format("", new ErrorFormatter(showTraces = true)))
//      case Failure(e) => {
//        println("Unexpected error during parsing: " + e); Left(e.getMessage) //println(e.format("", new ErrorFormatter(showTraces = true))); sys.error(e.format("", new ErrorFormatter(showTraces = true)))
//      }
//    }
  }
}