package dk.itu.vts.parser

/**
 * Created by scas on 25-09-2015.
 */

import dk.itu.vts.expression._
import dk.itu.vts.model.ProgamAST._
import dk.itu.vts.model._
import org.parboiled2._

import scala.util.Try

class TextlineParserParboiledBooleanExpressions(val input: ParserInput) extends Parser {
  def multiLineComment = rule {("/*" ~ zeroOrMore(!"*/" ~ ANY)) ~ "*/"}

  def ifdef = rule {ws ~ "#" ~ ws ~ "ifdef"}
  def ifndef = rule {ws ~ "#" ~ "ifndef"}
  def if_ = rule {ws ~ "#" ~ ws ~"if"}
  def else_ = rule {ws ~ "#" ~ ws ~ "else"}
  def elif = rule {ws ~ "#" ~ ws ~ "elif" }
  def endif = rule {ws ~ "#" ~ ws ~ "endif"}
  def endifRule = rule{ ws ~ "#" ~ ws ~"endif" ~ zeroOrMore(chars)}

  def ws = rule {zeroOrMore(" " | "\t")}
  def sep = rule{"\r\n" | "\n" | "\r"}

  def startRule = rule { inputLine ~ EOI}

  def inputLine: Rule1[Seq[Choiced[Textline]]] = rule { line.*(sep)}
  def elifblock_line: Rule1[Seq[Choiced[Textline]]] = rule {elifblock.*(sep)}

  def line = rule {  nochoice | choice}
  //def line = rule { ifdefVpChanged | nochoice | choice}

  //def ifdefVpChanged = rule {"#ifdef VPCHANGED___" ~ sep ~ nochoiceVpIf.*(sep) ~ optional("#else" ~ nochoiceVpIf.*(sep) ) ~ "#endif /* VPCHANGED___ */" ~> ((a: Program[Textline],b) => YourChoice(Defined("VPCHANGED___"), a, b))}

  //def nochoiceVpIf = rule {!(ws ~ ( else_ | endif )) ~ capture(zeroOrMore(chars))  ~> (x => NoChoice[Textline]((Textline[Choiced[Textline]](x))))}

  def comment = rule {capture(multiLineComment) ~> (x => x.split("\r?\n").toList map (y => NoChoice[Textline]((Textline[Choiced[Textline]](y)))))}
  def nochoice = rule {!(ws ~ (ifdef | ifndef | if_ | else_ | elif | endif )) ~ capture(zeroOrMore(chars))  ~> (x => NoChoice[Textline]((Textline[Choiced[Textline]](x))))}

  def ifExpression = rule{ws ~ capture((ifdef | ifndef | if_  ) ~ zeroOrMore(!"/*" ~ chars | multiLineComment)) ~> (x => ExpressionParserBoolean.parse(x.replaceAll("\r?\n", "")))}

  def choice = rule {emptyChoice | nonEmptyChoice}

  def nonEmptyChoice = rule {ifExpression ~ sep ~ inputLine ~ optional( sep ~ elseBlock) ~ sep ~ endifRule ~> ((a,b,c) => YourChoice(a match {
    case Some(x: Expression) => x
    case None => throw new RuntimeException("non-boolean-expression")
  }, b, c))}
  def emptyChoice = rule {ifExpression ~ optional(sep ~ elseBlock) ~ sep ~ endifRule ~> ((a: Option[Expression],b: Option[Program[Textline]]) => YourChoice(a match {
    case Some(x: Expression) => x
    case None => throw new RuntimeException("non-boolean-expression")
  }, Seq[Choiced[Textline]](), b)) }

  def elseBlock = rule {else_ ~ zeroOrMore(chars) ~ inputLine | &(elif) ~ elifblock_line }
  def elifblock: Rule1[YourChoice[Textline]] = rule {elifExpression ~ sep ~ inputLine ~ optional(sep ~ elseBlock)~> ((a: Option[Expression],b: Program[Textline],c: Option[Program[Textline]]) => YourChoice[Textline](a match {
    case Some(x: Expression) => x
    case None => throw new RuntimeException("non-boolean-expression")
  }, b,c))}
  def elifExpression = rule{ws ~ capture(elif ~ zeroOrMore(chars)) ~> (ExpressionParserBoolean.parse(_))}

  def chars = rule { UTF8_BOM|  CharPredicate.Printable | "\t" |deleteChar | generalPunctuation | formFeed | sharpS | extendedUTF | latin1Supplement | latinExtendedA | cyrillic  |controlCharacter1 | controlCharacter2 | controlCharacters | escape | arrows |  replacementCharacter | chinese | trademarkSign | mathOperators | greekAndCoptic | miscTechnical | katakana | hiragana | boxDrawning | datalinkEscape}
  val generalPunctuation = CharPredicate('\u2000' to '\u206F')
  val deleteChar = CharPredicate('\u007F')
  val quotes = CharPredicate('\u201D')
  val formFeed = CharPredicate('\u000C')
  val bullet = CharPredicate('\u2022')
  val bullet1 = CharPredicate('\u2B24')
  val sharpS = CharPredicate('\u00DF' )
  val weirdChars = CharPredicate('\u25A0' to '\u25FF')
  val extendedUTF = CharPredicate('\u00A0' to '\u00FC')
  val cyrillic = CharPredicate('\u0400' to '\u04FF') ++ CharPredicate('\u0500' to '\u052F') ++ CharPredicate('\uA640' to '\uA69F')
  val latinExtendedA = CharPredicate('\u0100' to '\u017F')
  val controlCharacters = CharPredicate('\u0011' to '\u0014')  // for Linux...
  val controlCharacter2 = CharPredicate('\u0008')  // for Busybox...
  val controlCharacter1 = CharPredicate('\u0001') // start of heading for MySQL
  val boxDrawning = CharPredicate('\u2500' to '\u257F')
  val escape = CharPredicate('\u001B')
  val arrows = CharPredicate('\u2190' to '\u21FF')
  val latin1Supplement = CharPredicate('\u00A0' to '\u00FF')
  val replacementCharacter = CharPredicate('\uFFFD')
  val chinese = CharPredicate('\u4E00' to '\u9FFF')
  val trademarkSign = CharPredicate('\u2122')
  val mathOperators = CharPredicate('\u2200' to '\u22FF')
  val greekAndCoptic = CharPredicate('\u0370' to '\u03FF')
  val miscTechnical = CharPredicate('\u2300' to '\u23FF')
  val katakana = CharPredicate('\u30A0' to '\u30FF')
  val hiragana = CharPredicate('\u3040' to '\u309F')
  val UTF8_BOM = CharPredicate('\uFEFF')
  val datalinkEscape = CharPredicate('\u0010')

}

object TextlineParserBooleanExpressions{
  def parseString(src: String): Try[Program[Textline]] = {
    new TextlineParserParboiledBooleanExpressions(src).startRule.run()
  }
}