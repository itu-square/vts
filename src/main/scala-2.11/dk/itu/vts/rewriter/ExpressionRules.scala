package dk.itu.vts.rewriter

import dk.itu.vts.expression._
import dk.itu.vts.model._
import dk.itu.vts.util.SatCheck
import org.kiama.rewriting.{Rewriter, Strategy}

import scala.language.higherKinds
/**
 * Created by scas on 12-11-2015.
 */
object ExpressionRules extends Rewriter with SatCheck{
  val trueValue = new java.math.BigInteger("1")
  def andOrExp: Strategy = reduce {
    rule[Expression] {
      case And(IntLiteral(trueValue), x) => x
      case And(x,IntLiteral(trueValue)) => x
      case Or(IntLiteral(trueValue), x) => x
      case Or(x,IntLiteral(trueValue)) => x
      case Or(And(x,y), z) if x == z || y == z => z
      case Or(z,And(x,y)) if x == z || y == z => z
      case And(Or(x,y),z) if x == z || y == z => z
      case And(z, Or(x,y)) if x == z || y == z => z
      case And(And(x,y),z) if x == z || y == z => And(x,y)
      case And(z,And(x,y)) if x == z || y == z => And(x,y)
      case Or(Or(x,y), z) if x == z || y == z => Or(x,y)
      case Or(z, Or(x,y)) if x == z || y == z => Or(x,y)
    }
  }

//    def pushConditions[T[_]] (expression: Expression) : Strategy = everywhere{
//        rule[YourChoice[T]]{
//          case YourChoice(expr: Expression, left: Program[T], right: Option[Program[T]]) if (expr != expression) => {
//            YourChoice(expr, rewrite(changeExpr(expr))(left), right match {case Some(x) => Some(rewrite(changeExpr(Negation(expr)))(x)); case None => None})
//          }
//        }
//    }

    def changeExpr[T[_]](expr: Expression, newExpr: Expression): Strategy = alltd {
      rule[YourChoice[T]] {
        case YourChoice(x: Expression, left, right) if (x != newExpr) => YourChoice(And(x, expr), left, right)
      }
    }

  def removeTrue[T[_]]: Strategy = alltd {
    rule [YourChoice[T]]{
      case YourChoice(And(True(), expr), left, right) => YourChoice(expr, left,right)
      case YourChoice(And(IntLiteral(trueValue), expr), left, right) => YourChoice(expr, left,right)
      case YourChoice(And(expr, True()), left, right) => YourChoice(expr, left,right)
      case YourChoice(And(expr, IntLiteral(trueValue)), left, right) => YourChoice(expr, left,right)
      case YourChoice(Or(True(), expr), left, right) => YourChoice(expr, left,right)
      case YourChoice(Or(expr, True()), left, right) => YourChoice(expr, left,right)
      case YourChoice(Or(IntLiteral(trueValue), expr), left, right) => YourChoice(expr, left,right)
      case YourChoice(Or(expr, IntLiteral(trueValue)), left, right) => YourChoice(expr, left,right)


    }
  }
}