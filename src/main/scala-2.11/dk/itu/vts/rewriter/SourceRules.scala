package dk.itu.vts.rewriter

import java.util.regex.Pattern

import dk.itu.vts.expression._
import dk.itu.vts.model.ProgamAST._
import dk.itu.vts.model._
import dk.itu.vts.util.{PrettyPrinter, SatCheck}
import org.kiama.rewriting.{Rewriter, Strategy}

import scala.language.higherKinds
/**
  * Created by scas on 03-11-2015.
  */
object SourceRules extends Rewriter with SatCheck {

  def transformToList[T[_]]: Strategy = innermost {
    rule[Program[T]] {
      case t: Vector[Choiced[T]] => t.toList
    }
  }

  def factoring[T[_]]: Strategy = innermost {
    rule[Program[T]] {
      case YourChoice(expr: Expression, left: Program[T], right: Option[Program[T]]) :: YourChoice(expr1: Expression, left1: Program[T], right1: Option[Program[T]]) +: t if (expr == expr1) => {
        YourChoice(expr, left ++ left1, {
          right match {
            case Some(x) => {
              right1 match {
                case Some(r1) => Some(x ++ r1)
                case None => Some(x)
              }
            }
            case None => right1 match {
              case Some(r1) => Some(r1)
              case None => None
            }
          }
        }) +: t
      }
      case YourChoice(expr: Expression, left: Program[T], right: Option[Program[T]]) :: YourChoice(expr1: Expression, left1: Program[T], right1: Option[Program[T]]) +: t if((expr == (Negation(expr1).removeDoubleNegation)) || ((Negation(expr).removeDoubleNegation) == expr1))  => {
        YourChoice(expr, left ++ {
          right1 match {
            case Some(x) => {x}
            case None => Seq()
          }}, right match {
          case Some(x) => Some(x ++ left1)
          case None => Some(left1)
        }) +: t
      }
    }
  }

  def ASTFactIdempotency[T[_]]: Strategy = innermost {
    rule[Program[T]] {
      case YourChoice(expr: Expression, left: Program[T], Some(right: Program[T])) +: t if left.nonEmpty && right.nonEmpty && (left.head == right.head) => Seq(left.head, YourChoice(expr, (left.tail), Some((right.tail)))) ++ t
      case YourChoice(expr: Expression, left: Program[T], Some(right: Program[T])) +: t if left.nonEmpty && right.nonEmpty && (left.last == right.last) => Seq(YourChoice(expr, (left.init), Some((right.init))), left.last) ++ t
      case Seq(YourChoice(expr: Expression, left: Program[T], Some(right: Program[T]))) if left.nonEmpty && right.nonEmpty && (left.head == right.head) => Seq(left.head, YourChoice(expr, (left.tail), Some((right.tail))))
      case Seq(YourChoice(expr: Expression, left: Program[T], Some(right: Program[T]))) if left.nonEmpty && right.nonEmpty && (left.last == right.last) => Seq(YourChoice(expr, (left.init), Some((right.init))), left.last)
    }
  }

  def idempotency[T[_]]: Strategy = innermost {
    rule[Program[T]] {
      case YourChoice(expr: Expression, left: Program[T], Some(right: Program[T])) +: t if left == right => left ++ t
      case Seq(YourChoice(expr: Expression, left: Program[T], Some(right: Program[T]))) if left == right => left
    }
//      case h +: t => h match {
//        case YourChoice(expr: Expression, left: Program[T], Some(right: Program[T])) if left == right => left ++ t
//        case _ => h +: t
//      }
//      case Vector(x) => x match {
//        case YourChoice(expr: Expression, left: Program[T], Some(right: Program[T])) if left == right => left
//        case _ => Seq(x)
//      }
//      case Vector() => Nil
//    }
  }

  // FIXME - make it to work on general ASTs. ?!?
//  def astFactoring[T[_]]: Strategy = everywherebu {
//    rule[Program[T]] {
//      case YourChoice(expr: Expression, left: Program[T], Some(right: Program[T])) :: t if (left.size == right.size) && left.nonEmpty && right.nonEmpty => left.zip(right).map(y => YourChoice(expr, Seq(y._1), Some(Seq(y._2)))) ++ t
//      case YourChoice(expr: Expression, left: Program[T], Some(right: Program[T])) +: t if (left.size == right.size) && left.nonEmpty && right.nonEmpty => left.zip(right).map(y => YourChoice(expr, Seq(y._1), Some(Seq(y._2)))) ++ t
//      case Seq(YourChoice(expr: Expression, left: Program[T], Some(right: Program[T]))) if (left.size == right.size) && left.nonEmpty && right.nonEmpty => left.zip(right).map(y => YourChoice(expr, Seq(y._1), Some(Seq(y._2))))
//      case List(YourChoice(expr: Expression, left: Program[T], Some(right: Program[T]))) if (left.size == right.size) && left.nonEmpty && right.nonEmpty => left.zip(right).map(y => YourChoice(expr, Seq(y._1), Some(Seq(y._2))))
//      //case s => s
//    }
//  }
//
//  def relaxedAstFactoring[T[_]]: Strategy = everywheretd {
//    rule[Program[T]] {
//      case YourChoice(expr: Expression, left: Program[T], Some(right: Program[T])) :: t => left.zip(right).map(y => YourChoice(expr, Seq(y._1), Some(Seq(y._2)))) ++ t
//      case s => s
//    }
//  }

  def flipNegation[T[_]]: Strategy = everywheretd {
    rule[YourChoice[T]] {
      case YourChoice(Negation(expr: Expression), Seq(), Some(right: Program[T])) => YourChoice(expr, right, None)
      case YourChoice(Negation(expr: Expression), left: Program[T], Some(right: Program[T])) if right.nonEmpty => YourChoice(expr, right, Some(left))
      case YourChoice(expr: Expression, Seq(), Some(right: Program[T])) if right.nonEmpty => YourChoice(Negation(expr), right, None)
    }
  }

  def flattenLeft[T[_]](input: Program[T], expr: Expression): Program[T] = input match {
    case YourChoice(e: Expression, left: Program[T], right: Option[Program[T]]) +: t => {
      if (e == expr) {
        flattenLeft(left, e) ++ flattenLeft(t, expr)
      }
      else if(e != expr && checkSat(e, expr)){
        YourChoice(e: Expression, flattenLeft(left, expr), right match{
          case Some(x) => Some(flattenLeft(x, expr))
          case None => None
        }) +: t
      }
      else if(e != expr && checkSat(Negation(e).removeDoubleNegation, expr)){
        YourChoice(e: Expression, flattenLeft(left, expr), right match{
          case Some(x) => Some(flattenLeft(x, expr))
          case None => None
        }) +: t
      }
      else if (!checkSat(e, expr)) {
        right match {
          case Some(Seq()) => flattenLeft(t, expr)
          case Some(y) => flattenLeft(y, e) ++ flattenLeft(t, expr)
          case None => flattenLeft(t, expr)
        }
      }
      else{
        right match {
          case Some(Seq()) => Seq(YourChoice(e, flattenLeft(left, expr), None)) ++ t
          case Some(x) => Seq(YourChoice(e, flattenLeft(left, expr), Some(flattenLeft(x, expr)))) ++ t
          case None => Seq(YourChoice(e, flattenLeft(left, expr), None)) ++ t
        }
      }//Seq(YourChoice(e, flattenLeft(left, expr), right)) ++ t

    }
    case Seq(YourChoice(e: Expression, left: Program[T], right: Option[Program[T]])) => {
      if (e == expr) {
        flattenLeft(left, e)
      }
      else if(e != expr && checkSat(e, expr)){
        Seq(YourChoice(e: Expression, flattenLeft(left, expr), right match{
          case Some(x) => Some(flattenLeft(x, expr))
          case None => None
        }))
      }
      else if(e != expr && checkSat(Negation(e).removeDoubleNegation, expr)){
        Seq(YourChoice(e: Expression, flattenLeft(left, expr), right match{
          case Some(x) => Some(flattenLeft(x, expr))
          case None => None
        }))
      }
      else if (!checkSat(e, expr)) {
        right match {
          case Some(y) => flattenLeft(y, expr)
          case None => Nil
        }
      }
      else{
        right match {
          case Some(Seq()) => Seq(YourChoice(e, flattenLeft(left, expr), None))
          case Some(x) => Seq(YourChoice(e, flattenLeft(left, expr), Some(flattenLeft(x, expr))))
          case None => Seq(YourChoice(e, flattenLeft(left, expr), None))
        }
      }
      //Seq(YourChoice(e, flattenLeft(left, expr), right))
  }
    case (x: NoChoice[T]) +: t => x +: flattenLeft(t, expr) // Vector
    case Seq(x: NoChoice[T]) => Seq(x) // Vector
    case Seq() => Nil
  }

  def flattenRight[T[_]](input: Program[T], expr: Expression): Program[T] = input match {
    case YourChoice(e: Expression, l: Program[T], r: Option[Program[T]]) +: t => {
      if (e == expr) {
        r match {
          case Some(y) => flattenRight(y, e) ++ flattenRight(t, expr);
          case None => flattenRight(t, expr)
        }
      }
        if(e == Negation(expr)) {
          r match {
            case Some(y) => flattenRight(y, e) ++ flattenRight(t, expr);
            case None => flattenRight(t, expr)
          }
        }
      else if (!checkSat(e, expr)) {
        flattenRight(l, e) ++ flattenRight(t, expr)
      }
      else Seq(YourChoice(e, l, {
        r match {
          case Some(y) => Some(flattenRight(y, e));
          case None => None
        }
      })) ++ flattenRight(t, expr)
    }
    case Seq(YourChoice(e: Expression, l: Program[T], r: Option[Program[T]])) => {
      if (e == expr) {
        r match {
          case Some(y) => flattenRight(y, e);
          case None => Nil
        }
      }
      else if (!checkSat(e, expr)) {
        flattenRight(l, e)
      }
      else Seq(YourChoice(e, l, {
        r match {
          case Some(y) => Some(flattenRight(y, e));
          case None => None
        }
      }))
    }
    case (x: NoChoice[T]) +: t => x +: flattenRight(t, expr) // Vector
    case Seq(x: NoChoice[T]) => Seq(x)
    case Seq() => Nil
  }




  def choiceDomination[T[_]]: Strategy = everywherebu {

    rule[YourChoice[T]] {
      case YourChoice(expr: Expression, left: Program[T], Some(right: Program[T])) => {
        YourChoice(expr, flattenLeft(left, expr), Some(flattenRight(right, expr.removeDoubleNegation)))
      }
      case YourChoice(expr: Expression, left: Program[T], right: Option[Program[T]]) => {
        YourChoice(expr, flattenLeft(left, expr), right match { case Some(y) => Some(flattenRight(y, expr.removeDoubleNegation)); case None => None })
      }
    }
  }

  import dk.itu.vts.smt.SMT.checkSAT
  def choiceDomA[T[_]]: Strategy = everywheretd  {
    rule[Program[T]] {
      case YourChoice(expr: Expression, left: Program[T], right: Option[Program[T]]) +: t => {
        def changeChoice[T[_]](e: Expression): Strategy = reduce{
          rule[Program[T]] {
            case YourChoice(ex: Expression, l: Program[T], Some(r: Program[T])) +: t if (e == ex) => l ++ t
            case YourChoice(ex: Expression, l: Program[T], None) +: t if (e == ex) => l ++ t
            case YourChoice(ex: Expression, l: Program[T], None) +: t if (Negation(e).removeDoubleNegation == ex) => t
            case YourChoice(ex: Expression, l: Program[T], Some(r: Program[T])) +: t if (!checkSAT(Negation(ex).removeDoubleNegation, expr))=> r ++ t
            case YourChoice(ex: Expression, l: Program[T], Some(r: Program[T])) +: t if (Negation(ex).removeDoubleNegation == expr)=> r ++ t
          }
        }
        val newLeft = (rewrite(changeChoice(expr))(left))
        val newRight = right match{
          case Some(x) => Some(rewrite(changeChoice(Negation(expr)))(x))
          case None => None
        }
        YourChoice(expr, newLeft, newRight) +: t
      }
    }
  }

//  def choiceDomWithRewriting[T[_]]: Strategy = {
//
//    def firstChoice: Strategy = everywherebu {
//      rule[YourChoice[T]]{
//        case YourChoice(expr: Expression, left: Program[T], right: Option[Program[T]]) => {
//          YourChoice(expr: Expression, executeLeftInnerChoices(expr, left), right match {
//            case Some(x) => Some(executeRightInnerChoices(Negation(expr).removeDoubleNegation,x))
//            case None => None
//          })
//        }
//      }
//    }
//    def leftInnerChoices(expr: Expression): Strategy = everywherebu {
//      rule[YourChoice[T]]{
//        case YourChoice(ex: Expression, left: Program[T], right: Option[Program[T]]) if (ex == expr) => YourChoice(True(),left, None)
//        case YourChoice(ex: Expression, left: Program[T], right: Option[Program[T]])  if (!checkSat(ex, expr)) => YourChoice(True(), right match {case Some(x: Program[T]) => x; case None => Seq()}, None)
//        case YourChoice(ex: Expression, left: Program[T], right: Option[Program[T]])  if (checkSat(ex, expr)) => YourChoice(ex, left, right)
//      }
//    }
//
//    def rightInnerChoice(expr: Expression): Strategy = everywherebu {
//      rule[Choiced[T]]{
//        case YourChoice(ex: Expression, left: Program[T], right: Option[Program[T]]) if (ex == expr) => YourChoice(True(),left, None)
//        case YourChoice(ex: Expression, left: Program[T], right: Option[Program[T]])  if (!checkSat(ex, expr)) => YourChoice(True(), right match {case Some(x: Program[T]) => x; case None => Seq()}, None)
//        case YourChoice(ex: Expression, left: Program[T], right: Option[Program[T]])  if (checkSat(ex, expr)) => YourChoice(ex, left, right)
//      }
//    }
//
//    def executeRightInnerChoices(e: Expression, ast: Program[T]) = rewrite(rightInnerChoice(e))(ast)
//    def executeLeftInnerChoices (e: Expression, ast: Program[T]) = rewrite(leftInnerChoices(e))(ast)
//    firstChoice
//  }

  def flattenTrue[T[_]]: Strategy = everywherebu {
    rule[Program[T]] {
      //case YourChoice(e, Seq(), right) +: t => t
      case YourChoice(True(), left, right) +: t => left ++ t
      case YourChoice(IntLiteral(trueValue), left, right) +: t=> left ++ t
//      case t => t.foldRight(Seq[Choiced[T]]()) { (choice, rest) =>
//        choice match {
//          case YourChoice(True(), left, right) => left ++ rest
//          case YourChoice(IntLiteral(trueValue), left, right) => left ++ rest
//          case _ => choice +: rest
//        }
//      }
    }
  }

  // THIS seems to fail in some cases. Use choiceDomA
//  def choiceDom[T[_]]: Strategy = {
//
//    def choices: Strategy = everywheretd {
//      rule[YourChoice[T]] {
//        case YourChoice(expr: Expression, left: Program[T], Some(right: Program[T])) => YourChoice(expr, rewrite(dominate(expr))(left), Some(rewrite(dominate(Negation(expr).removeDoubleNegation))(right)))
//        case YourChoice(expr: Expression, left: Program[T], None) => YourChoice(expr, rewrite(dominate(expr))(left), None)
//      }
//    }

//    def dominate(ex: Expression): Strategy = innermost {
//      rule[Program[T]] {
////        case YourChoice(expr: Expression, left: Program[T], right: Option[Program[T]]) +: Seq() if (ex == expr) => left
////        case YourChoice(expr: Expression, left: Program[T], right: Option[Program[T]]) +: Seq() if (!checkSat(ex, expr)) => right match {
////          case Some(y) => y
////          case None => Nil
////        }
//        case YourChoice(expr: Expression, left: Program[T], right: Option[Program[T]]) +: t if (ex == expr) => left ++ t
//        case YourChoice(expr: Expression, left: Program[T], right: Option[Program[T]]) +: t if (ex == Negation(expr).removeDoubleNegation) => {
//          right match {case Some(x) => x ++ t; case None => t}
//        }
//        case YourChoice(expr: Expression, left: Program[T], right: Option[Program[T]]) +: t if (ex != expr && checkSat(ex,expr)) => {
//          YourChoice(expr: Expression, rewrite(dominate(ex))(left), right match{case Some(x) => Some(rewrite(dominate(ex))(x)); case None => None}) +: t
//        }
//        case YourChoice(expr: Expression, left: Program[T], right: Option[Program[T]]) +: t if (!checkSat(ex, expr)) => right match {
//          case Some(y) => y++ t//YourChoice(expr: Expression, left: Program[T], Some(y)) +: t
//          case None => t
//        }
//
//        case Seq(YourChoice(expr: Expression, left: Program[T], right: Option[Program[T]])) if (ex == expr) => left
//        case Seq(YourChoice(expr: Expression, left: Program[T], right: Option[Program[T]])) if (!checkSat(ex, expr)) => right match {
//          case Some(y) => Seq(YourChoice(expr: Expression, left: Program[T], Some(y)))
//          case None => Nil
//        }
//
//      }
//    }
//
//    choices
//  }

  def joinOr[T[_]]: Strategy = everywherebu {
    rule[YourChoice[T]] {
      case YourChoice(expr: Expression, left: Program[T], Some(Seq(YourChoice(elseNestedExpr: Expression, elseNestedleft: Program[T], elseNestedRight: Option[Program[T]])))) if (left == elseNestedleft) => {
        YourChoice(Or(expr, elseNestedExpr), left, elseNestedRight)
      }
    }
  }

  def joinAnd[T[_]]: Strategy = everywherebu {
    rule[YourChoice[T]] {
      case YourChoice(expr: Expression, Seq(YourChoice(ifNestedExpr: Expression, ifNestedleft: Program[T], ifNestedRight: Option[Program[T]])), right: Option[Program[T]]) if (ifNestedRight == right) => {
        YourChoice(And(expr, ifNestedExpr), ifNestedleft, right)
      }
    }
  }

  def joinOrNot[T[_]]: Strategy = everywherebu {
    rule[YourChoice[T]] {
      case YourChoice(expr: Expression, left: Program[T], Some(Seq(YourChoice(elseExpr: Expression, elseLeft: Program[T], Some(elseRight: Program[T]))))) if (left == elseRight) => {
        YourChoice(Or(expr, Negation(elseExpr)), left, Some(elseLeft))
      }
    }
  }

  def joinAndNot[T[_]]: Strategy = everywherebu {
    rule[YourChoice[T]] {
      case YourChoice(expr: Expression, Seq(YourChoice(ifNestedExpr: Expression, ifNestedLeft: Program[T], ifNestedRight: Option[Program[T]])), Some(right: Program[T])) if (ifNestedLeft == right) => {
        YourChoice(And(expr, Negation(ifNestedExpr)), ifNestedRight match { case Some(x) => x; case None => Seq() }, Some(right))
      }
    }
  }

  def newJoin[T[_]]: Strategy = {

    def myNotSameZip[T[_]](left: Program[T], right: Program[T]): Option[Tuple5[Program[T],Program[T],Choiced[T],Program[T],Program[T]]] = {
      val firstCommonElement = left.intersect(right).headOption
      firstCommonElement match {
        case Some(x) => {
          val index = left.indexOf(x)
          val left_split = left.splitAt(index)
          val right_split = right.splitAt(right.indexOf(x))
          Some(Tuple5(left_split._1, right_split._1, (left_split._2.head), left_split._2.tail, right_split._2.tail))
        }
        case None => None
      }
    }
    def innerJ[T[_]]: Strategy = everywherebu{
      rule[Program[T]]{
        case YourChoice(expr: Expression, left: Program[T], Some(right: Program[T])) +: t => {
          val zip = myNotSameZip(left, right)
          zip match {
            case Some(Tuple5(a,b,c,d,e)) => Seq(YourChoice(expr, a, Some(b)),c,YourChoice(expr,d,Some(e))) ++ t
            case None => YourChoice(expr, left,Some(right)) +: t
          }

        }
      }
    }
    topdown (innerJ <* ASTFactIdempotency)
  }
  // FIXME - this does not seem to be correct
  def join[T[_]]: Strategy = {

    def innerJ[T[_]]: Strategy = everywherebu {
      rule[YourChoice[T]] {
        case YourChoice(expr: Expression, Seq(YourChoice(lExpr: Expression, left: Program[T], right: Option[Program[T]])), Some(Seq(YourChoice(rExpr: Expression, rLeft: Program[T], rRight: Option[Program[T]])))) if lExpr == rExpr => {
          val zip = myZipSame(left, rLeft)
          YourChoice(lExpr, zip._1 ++ Seq(YourChoice(expr, zip._2, Some(zip._3))), right)
        }
      }
    }
    topdown(innerJ <* ASTFactIdempotency)
  }

  /**
    * Return a triple where first element is the list with the same elements
    * of the two lists starting from first. The second element returns the
    * elements from the left list from the point where the left and right lists
    * have different elements, and the third element is the remaining
    *
    * @param left
    * @param right
    * @tparam T
    * @return
    */
  def myZipSame[T](left: Seq[T], right: Seq[T]) = {
    var i = 0
    while ((left.size > i && right.size > i) && left(i) == right(i))
      i = i + 1

    val leftResult = left.splitAt(i)
    (leftResult._1, leftResult._2, right.splitAt(i)._2)
  }

  def myZipNotSame[T](left: Seq[T], right: Seq[T]) = {

    var h = left
    var newHead = Seq[T]()

    while(h.nonEmpty && h.head != right.head ){ // the problem here is if it finds empty lines, then it will split; this is fixed in a different rule that hacks everything back together if there are empty lines between #ifdefs with same PC
      // trying to hack out the part of having whitespaces (blank lines) between code

      newHead = newHead :+ h.head
      h = h.tail
    }
    (newHead,left.diff(newHead),right)
  }

  /**
    * This rule is used when a choice has different nodes but also common nodes in the left and right branches, and tries to hoist them
    *
    * @tparam T
    * @return
    */
  def astFACTIdemp[T[_]]: Strategy = {

    def innerZip: Strategy = everywherebu {
      rule[Program[T]] {
        case YourChoice(expr, left, Some(right: Program[T])) +: t if left.nonEmpty && right.nonEmpty => {//&& left.contains(right.head) => {
//          print("LEFT: "); println(left)
//          println("RIGHT: "); println(right)
          val zip = myZipNotSame(left, right)
          Seq(YourChoice(expr, zip._1, None), YourChoice(expr, zip._2, Some(zip._3))) ++ t
        }
      }
    }
    topdown(innerZip <* ASTFactIdempotency)
  }

  def rejoin[T[_]]: Strategy = everywheretd{
    rule[Program[T]] {
      case YourChoice(expr: Expression, left: Program[T], right: Option[Program[T]]) :: (x: NoChoice[T]) :: YourChoice(expr1: Expression, l: Program[T], r: Option[Program[T]]) +: t if (expr == expr1) => {
        val regex = Pattern.compile("""(\s*)|(\r?\n)""").matcher("")
        val s = regex.reset(x.t.toString).matches()
        s match {
          case true => YourChoice(expr, left ++ Seq(x) ++ l, right match {
            case Some(xs) => r match {
              case Some(ys) => Some( xs ++ ys)
              case None => Some(xs)
            }
            case None => r match {
              case Some(ys) => Some(ys)
              case None => None
            }
          }) +:t
          case false => YourChoice(expr: Expression, left: Program[T], right: Option[Program[T]]) :: x :: YourChoice(expr1: Expression, l: Program[T], r: Option[Program[T]]) +: t
        }
      }
    }
  }


  def removeEmptyBranches[T[_]]: Strategy = innermost {
    rule[Program[T]] {
      case YourChoice(expr, Seq(), None) +: tail => tail
      case Seq(YourChoice(expr, Seq(), None)) => Seq()
      case YourChoice(expr, Seq(), Some(Seq())) +: tail => tail
      case Seq(YourChoice(expr, Seq(), Some(Seq()))) => Seq()
      case YourChoice(expr, left: Program[T], Some(Seq())) +: tail => YourChoice(expr, left, None) +: tail
      case Seq(YourChoice(expr, left: Program[T], Some(Seq()))) => Seq(YourChoice(expr, left, None))
      case YourChoice(expr, Seq(), Some(right: Program[T])) +: tail => YourChoice(Negation(expr).removeDoubleNegation, right, None) +: tail
      case Seq(YourChoice(expr, Seq(), Some(right: Program[T]))) => Seq(YourChoice(Negation(expr).removeDoubleNegation, right, None))
    }
  }

  def nestedIfdefs[T[_]]: Strategy = everywherebu{
    rule[Program[T]]{
      case YourChoice(expr, left: Program[T], Some(right: Program[T]))  +: t if (!left.isEmpty && !right.isEmpty) => {
        val leftLast = left.last
        val rightLast = right.last

        if (leftLast.isInstanceOf[YourChoice[T]] && rightLast.isInstanceOf[YourChoice[T]]){
          val leftChoice = leftLast.asInstanceOf[YourChoice[T]]
          val rightChoice = rightLast.asInstanceOf[YourChoice[T]]

          if (leftChoice.expr == rightChoice.expr){

            val s = Seq(YourChoice(expr, leftChoice.left, Some(rightChoice.left)))
            //Seq(YourChoice(leftChoice.expr,leftChoice.left, Some(rightChoice.left)))
            val r = rewriteAST(s)
            val result = YourChoice(leftChoice.expr, r, None)
            Seq(YourChoice(expr, left.init, Some(right.init)),result) ++ t
            //YourChoice(expr, left.init ++ r, Some(right.init)) +:  t
          }
          else {
            YourChoice(expr, left: Program[T], Some(right: Program[T]))  +: t
          }
        }
        else {
          YourChoice(expr, left: Program[T], Some(right: Program[T]))  +: t
        }
      }
    }
  }

  def rewriteAST[T[_]](ast: Program[T]) = {
    import dk.itu.vts.rewriter.ExpressionRules.{andOrExp, removeTrue}
    def allR (AST: Program[T]) = rewrite(
          andOrExp <*
          removeTrue <*
          flattenTrue <*
          //flipNegation <*
          idempotency <*
          choiceDomA <*
          ASTFactIdempotency <*
          joinOr <*
          joinAnd <*
          joinAndNot <*
          joinOrNot <*
          join <*
          choiceDomA <*
          ASTFactIdempotency <*
          astFACTIdemp <* // FIXME this seems to create bad code sometimes when whitespaces are used.
//            nestedIfdefs <*
          factoring <*
          choiceDomA <*

          ASTFactIdempotency <*
          rejoin <*
          newJoin <*
          removeEmptyBranches
        )(AST)

    // make any Vector to List
    val normalizeAST = rewrite(transformToList)(ast)

    //val first = allR(normalizeAST)
    //val second = rewrite(choiceDomWithRewriting)(first)
    allR(normalizeAST)
  }
}