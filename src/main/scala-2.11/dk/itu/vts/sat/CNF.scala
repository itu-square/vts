package dk.itu.vts.sat

/*
  Copied from https://code.google.com/p/linux-variability-analysis-tools/
  23.05.2015
 */
import dk.itu.vts.expression._
import org.kiama.rewriting.Rewriter._
import org.kiama.rewriting.Strategy
import sat.Clause

import scala.collection.mutable.HashMap

object IdMap {
  def apply(es: Iterable[Expression]): Map[String, Int] = {
    val map = new HashMap[String, Int]
    es foreach { e =>
      e.identifiers filter { !map.contains(_) } foreach { id =>
        map += id -> (map.size + 1)
      }
    }
    Map() ++ map
  }
}


object CNFBuilder {

  val sDistributeRule: Strategy = oncetd {
    rule [Expression]{
      case Or(And(x,y),z) => And(Or(x,z), Or(y,z))
      case Or(x,And(y,z)) => And(Or(x,y), Or(x,z))
    }
  }

  val sIffRule = everywheretd {
    rule [Expression]{
      case BIff(x,y) => (!x | y) & (!y | x)
    }
  }

  val sImpliesRule = everywheretd {
    rule [Expression] {
      case Implies(x,y) => !x | y
    }
  }
  val demorgansRule = reduce {
    rule [Expression]{
      case Negation(And(x,y)) => Or(Negation(x), Negation(y))
      case Negation(Or(x,y)) => And(Negation(x), Negation(y))
    }
  }

  /**
   * Run until we reach a fixpoint.
   */
  def distribute(e: Expression): List[Expression] = {
    val result = rewrite(sDistributeRule)(e)
    if (result == e) result.splitConjunctions
    else result.splitConjunctions flatMap distribute
  }

  /**
   * @param idMap Maps identifiers in the expression to an integer
   */
  def toClause(e: Expression, idMap: collection.Map[String, Int]): Clause = e match {
    case True() => List(idMap("true"))
    case False() => List(idMap("false"))
    case Negation(Negation(x)) => toClause(x, idMap)
    case Negation(Identifier(v)) => List(-idMap(v))
    case Negation(Defined(v)) => List(-idMap(v))
    //case Negation(And(l,r)) => toClause(Or(Negation(l), Negation(r)), idMap)
    case Or(x, y) => toClause(x, idMap) ++ toClause(y, idMap)
    case And(True(), y) => toClause(True(), idMap) ++ toClause(y, idMap)
    case And(False(), y) => toClause(False(), idMap) ++ toClause(y, idMap)
    case And(x, True()) => toClause(x, idMap) ++ toClause(True(), idMap)
    case And(x, False()) => toClause(x, idMap) ++ toClause(False(), idMap)
    case And(x, y) => toClause(x, idMap) ++ toClause(y, idMap)

    //case LessThan(x,y) => toClause(x, idMap) ++ toClause(y, idMap)
    //case LessThanOrEquals(x,y) => toClause(x, idMap) ++ toClause(y, idMap)
    //case GreaterThan(x,y) => toClause(x, idMap) ++ toClause(y, idMap)
    //case GreaterThanOrEquals(x,y) => toClause(x, idMap) ++ toClause(y, idMap)
    //case EqualsTo(Negation(x),y) if (x.isInstanceOf[Defined]) => List(-idMap(x.asInstanceOf[Defined].name)) ++ toClause(y, idMap)
    //case EqualsTo(x,y) => toClause(x, idMap) ++ toClause(y, idMap)

    //case Negation(x) => toClause(x, idMap)
    case Identifier(v) => List(idMap(v))
    case Defined(v) => List(idMap(v))
    //case IntLiteral(v) => List(idMap(v.toString()))
    //case DecimalLiteral(v) =>List(idMap(v.toString()))
    //case FloatLiteral(v) => List(idMap(v.toString))
    //case UnaryMinus(e) => toClause(e, idMap)
    //case UnaryPlus(e) => toClause(e, idMap)
    //case Mod(x,y) => toClause(x, idMap) ++ toClause(y, idMap)
      // FIXME - this needs to be refactored!!!!!!!!
//    case Negation(EqualsTo(x,y)) if (x.isInstanceOf[Defined] && y.isInstanceOf[IntLiteral]) => {
//
//      if ( 1 == y.asInstanceOf[IntLiteral].value.intValue())
//        toClause(Negation(x),idMap) // ++ toClause((x),idMap)
//      else
//        toClause((x.asInstanceOf[Defined]),idMap) //++ toClause(Negation(x),idMap)
//    }
//    case Negation(EqualsTo(x,y)) if (x.isInstanceOf[Identifier] && y.isInstanceOf[IntLiteral]) => {
//      if ( 1 == y.asInstanceOf[IntLiteral].value.intValue())
//        toClause(Negation(x),idMap) // ++ toClause((x),idMap)
//      else
//        toClause((x.asInstanceOf[Identifier]),idMap) //++ toClause(Negation(x),idMap)
//    }
//    case Negation(EqualsTo(x,y)) if (x.isInstanceOf[Identifier] && y.isInstanceOf[Identifier]) => {
//      if ( x == y)
//        toClause(Negation((x)),idMap) ++ toClause((y),idMap)
//      else
//        toClause(((x)),idMap) ++ toClause((y),idMap)
//    }
    case _ => sys.error("Wrong format. Expression is not a clause: " + e)
  }

  /**
   * @param idMap Maps identifiers in the expression to an integer
   */
  def toCNF(e: Expression, idMap: collection.Map[String, Int]) =
    rewrite(demorgansRule <* sIffRule <* sImpliesRule)(e)
        .simplify
        .splitConjunctions
        .filter { _ != True() }
        .flatMap { distribute }
        .map { toClause(_, idMap) }

}