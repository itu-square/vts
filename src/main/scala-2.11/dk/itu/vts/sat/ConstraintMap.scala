package dk.itu.vts.sat

/*
  Copied from https://code.google.com/p/linux-variability-analysis-tools/
  23.05.2015
 */
import org.sat4j.specs.IConstr
import sat.Clause

import scala.collection.mutable.HashMap

trait ConstraintMap extends SATBuilder {
  
  val constraints = new HashMap[IConstr, Clause]

  override def constrToClause(constr: IConstr, clause: Clause) =
    constraints += constr -> clause

}