/*
 * This file is part of the Linux Variability Modeling Tools (LVAT).
 *
 * Copyright (C) 2010 Steven She <shshe@gsd.uwaterloo.ca>
 *
 * LVAT is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LVAT is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LVAT.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */
/*
  Copied from https://code.google.com/p/linux-variability-analysis-tools/
  23.05.2015
 */
package dk.itu.vts.sat

import dk.itu.vts.expression.{Expression, False, True}

import scala.language.implicitConversions

/**
 * Convenience methods for Lists containing Exprs.
 *
 * @author Steven She (shshe@gsd.uwaterloo.ca)
 */

trait ExprList {

  implicit def toExprList(lst : List[Expression]) =
    new ExprList(lst)

  class ExprList(lst : List[Expression]) {
    def mkDisjunction = lst match {
      case Nil => False()
      case _ => lst.reduceLeft(_ | _)
    }
    def mkConjunction = lst match {
      case Nil => True()
      case _ => lst.reduceLeft(_ & _)
    }
  }

}