package dk.itu.vts.sat

/*
  Copied from https://code.google.com/p/linux-variability-analysis-tools/
  23.05.2015
 */
import org.sat4j.specs.ISolver
import org.sat4j.tools.xplain.Xplain
import sat.Clause

import scala.collection.JavaConversions

trait XplainSupport extends SATSolver[Xplain[ISolver]] {
  this: ConstraintMap =>

  import JavaConversions._

  val xplain = new Xplain[ISolver](super.newSolver)
  
  abstract override def newSolver = xplain

  /**
   * Returns a list of clauses that causes an unsatisfiable result.
   * Can only be called after the solver returns !isSatisfiable.
   */
  def explain: List[Clause] =
    xplain.explain.toList map constraints.apply

}