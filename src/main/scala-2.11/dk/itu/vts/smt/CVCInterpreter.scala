package dk.itu.vts.smt

/**
  * Copied from Ahmad Salim Al-Sibahi veriTRON, 26.02.2016
  * All copyrights to him for the following code
  *
  */
import smtlib.interpreters._

object CVCInterpreter {

  val defaultArgs: Array[String] =
    Array("-q",
      "--no-incremental",
      "--dt-rewrite-error-sel",
      "--print-success",
      "--lang", "smt2.5")

  def build(args: Array[String] = defaultArgs) = {
    val executable = "cvc4"
    new CVC4Interpreter(executable, args)
  }
}