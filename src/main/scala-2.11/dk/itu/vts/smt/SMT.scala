package dk.itu.vts.smt

/**
  * Created by scas on 26-02-2016.
  */

import dk.itu.vts.expression.{Defined, DefinedValue, Expression}
import org.kiama.rewriting.Rewriter._
import org.kiama.rewriting.Strategy
import smtlib.common.Hexadecimal
import smtlib.interpreters.Z3Interpreter
import smtlib.parser.Commands._
import smtlib.parser.CommandsResponses.SatStatus
import smtlib.parser.Terms.{Term, _}
import smtlib.theories.Core._
import smtlib.theories.Ints.{IntSort, NumeralLit}

import scala.language.postfixOps


object SMT {

  /**
    * Copied from Ahmad Salim Al-Sibahi veriTRON
    * All copyrights to him for the following code
    *
    */
  private val prelogue =
    List(SetLogic(QF_NIA))// StandardLogic(SSymbol("QF_NIA "))))

  private val epilogue = List(CheckSat())

  private def makeScript(subscripts: List[Command]*) =
    Script(prelogue ++ subscripts.fold(List())(_ ++ _) ++ epilogue)
  /**/

  def isInExpression(symbol: String, exprs: List[String]) = {
    exprs.exists(_ == symbol)
  }


  def asserts (expr: Expression, symbmap: Map[String, (SSymbol, Sort)]): Term = expr match {
    case dk.itu.vts.expression.And(x,y) => smtlib.theories.Core.And(asserts(x, symbmap), asserts(y,symbmap))
    case dk.itu.vts.expression.Or(x,y) => Or(asserts(x,symbmap), asserts(y,symbmap))
    case dk.itu.vts.expression.Negation(x) => smtlib.theories.Core.Not(asserts(x,symbmap))
    case dk.itu.vts.expression.GreaterThan(x,y) => smtlib.theories.Ints.GreaterThan(asserts(x,symbmap), asserts(y,symbmap))
    case dk.itu.vts.expression.GreaterThanOrEquals(x,y) => smtlib.theories.Ints.GreaterEquals(asserts(x,symbmap), asserts(y,symbmap))
    case dk.itu.vts.expression.LessThan(x,y) => smtlib.theories.Ints.LessThan(asserts(x,symbmap), asserts(y,symbmap))
    case dk.itu.vts.expression.LessThanOrEquals(x,y) => smtlib.theories.Ints.LessEquals(asserts(x,symbmap), asserts(y,symbmap))
    case dk.itu.vts.expression.EqualsTo(x,y) => smtlib.theories.Core.Equals(asserts(x,symbmap), asserts(y,symbmap))
    case dk.itu.vts.expression.NotEqualsTo(x,y) => smtlib.theories.Core.Not(smtlib.theories.Core.Equals(asserts(x,symbmap), asserts(y,symbmap)))

    case dk.itu.vts.expression.Defined(x) => QualifiedIdentifier(Identifier(symbmap.get(x).get._1))
    case dk.itu.vts.expression.Identifier(x) => QualifiedIdentifier(Identifier(symbmap.get(x).get._1))
    case dk.itu.vts.expression.True() => smtlib.theories.Core.True()
    case dk.itu.vts.expression.False() => smtlib.theories.Core.False()
    case dk.itu.vts.expression.UnaryMinus(x) => smtlib.theories.Ints.Neg(NumeralLit(BigInt(x.toString)))
    case dk.itu.vts.expression.IntLiteral(x) => SNumeral(x)
    case dk.itu.vts.expression.HexLiteral(x) => SHexadecimal(Hexadecimal.fromString(x).get)
    case dk.itu.vts.expression.StringLiteral(x) => SString(x)
    case dk.itu.vts.expression.FloatLiteral(x) => SDecimal(BigDecimal(x))
    case dk.itu.vts.expression.DecimalLiteral(x) => SDecimal(BigDecimal(x))
  }


  def makeSymbolsAndDecl(a: Expression, b: Expression) = {
    val symbmap = makeMap(a) ++ makeMap(b)
    val nonBooleanExprsIdentifiers = (collectNonBooleanExpr(a)  ++ collectNonBooleanExpr(b)).toList.map (x => x.identifiersForMetrics) flatten

    val symbdecl = symbmap.values.toList.map(sym => {
      // FIXME: this should be done according to the type of the right hand expression: int -> IntSort; float -> RealSort
      if (isInExpression(sym._1.name, nonBooleanExprsIdentifiers))
        DeclareFun(sym._1, Seq(), IntSort()): Command
      else DeclareFun(sym._1, Seq(), sym._2): Command
    })
    (symbmap, symbdecl)

  }
  def checkImplies(a: Expression, b: Expression): Boolean = {
    val symbsAndDecl = makeSymbolsAndDecl(a,b)
    val symbmap = symbsAndDecl._1
    val symbdecl = symbsAndDecl._2
    val verifyImplication = List(Assert(Or(Not(asserts(a,symbmap)),asserts(b,symbmap))))
    val script = makeScript(symbdecl,verifyImplication)

    interpret(script)
  }


  def checkSAT(projection: Expression,expr: Expression): Boolean = {
    val transformedExpressions = transformDefinedValues(projection,expr)
    val transformedA = transformedExpressions._1
    val transformedB = transformedExpressions._2

    val symbsAndDecl = makeSymbolsAndDecl(transformedA,transformedB)
    val symbmap = symbsAndDecl._1
    val symbdecl = symbsAndDecl._2
    val verifyAsserts = List(Assert(And(asserts(transformedA,symbmap),asserts(transformedB,symbmap))))
    val script = makeScript(symbdecl,verifyAsserts)
//    println(script)
    interpret(script)
  }

  def transformDefinedValues(projection: Expression, expr: Expression) = {
    def rewriteIdentifiers(e: DefinedValue): Strategy = reduce {
      rule[Expression] {
        case dk.itu.vts.expression.Identifier(x) if (x == e.name) => dk.itu.vts.expression.IntLiteral(e.value)
      }
    }

    def collectDefinedValues(e: Expression) = collects {
      case x: DefinedValue => x
    }(e)

    def rewriteExpression(e: Expression): Strategy = reduce {
      rule[Expression] {
        case x: DefinedValue if x == e=> Defined(x.name)
      }
    }
    def rewriteExpressionToNegated(e: Expression): Strategy = reduce {
      rule[Expression] {
        case x: DefinedValue if x == e=> dk.itu.vts.expression.Negation(Defined(x.name))
      }
    }
  /*
    val defineValues = collectDefinedValues(partial_projection)
    var newExpr = expr
    var newProjection = partial_projection
    defineValues.foreach(x => {
      if(x.asInstanceOf[DefinedValue].value !=0){
        newExpr = rewrite(rewriteIdentifiers(x))(newExpr)
        newProjection = rewrite(rewriteExpression(x))(partial_projection)
      }
      else {
        newExpr = rewrite(rewriteIdentifiers(x))(newExpr)
        newProjection = rewrite(rewriteExpressionToNegated(x))(partial_projection)
      }

    })
    (newProjection,newExpr)*/
    (projection,expr)

  }

  def interpret(script: Script) = {
    var interpreter: ScriptInterpreter = null
    try {
      interpreter = ScriptInterpreter(Z3Interpreter.buildDefault)
//      interpreter = ScriptInterpreter(CVCInterpreter.build(CVCInterpreter.defaultArgs))
      val res = interpreter.interpret(script)
      interpreter.satStatus(res).fold(false) {
        case SatStatus => true
        case s => false
      }
    }
    catch {
      case ex: Exception => println(ex.getMessage); sys.error("Error")
    }
  }

  import org.kiama.rewriting.Rewriter._
  def collectExpr(e: Expression): Set[String] =
    collects {
      case Defined(x) => x
      //case DefinedValue(x,v) => x
      case dk.itu.vts.expression.Identifier(x) => x
    }(e)


  def collectIdentifiers(exprs: Set[Expression]) = {
    for (ex <- exprs) yield collectExpr(ex) flatten
  }

  def collectNonBooleanExpr(e: Expression) = collects {
    case x: dk.itu.vts.expression.GreaterThan => x
    case x: dk.itu.vts.expression.GreaterThanOrEquals => x
    case x: dk.itu.vts.expression.LessThan => x
    case x: dk.itu.vts.expression.LessThanOrEquals => x
    case x: dk.itu.vts.expression.EqualsTo => x
    case x: dk.itu.vts.expression.NotEqualsTo => x
  }(e)

  def makeMap(ex: Expression) = {
    collectExpr(ex).toList.map(x => (x-> (SSymbol(x),BoolSort()))).toMap
  }
}