package dk.itu.vts.util

import java.io.{File, FileNotFoundException}
import java.nio.file.FileSystemNotFoundException

import scala.annotation.tailrec

/**
 * Created by scas on 24-09-2015.
 */
object FileNormalizer {
  val ifRegex = java.util.regex.Pattern.compile("""\s*\#\s*if\s*.*\\\s*""").matcher("")
  val elifRegex = java.util.regex.Pattern.compile("""\s*\#\s*elif\s*.*\\\s*""").matcher("")
  /**
   * Normalizes a file and returns a string of the normalized file
   * @param file
   * @return
   */

  def normalizeFile(file: String, encoding: String) = {
    val txtFile = {
      try{
        //FileUtils.readLines(new File(file),encoding)
        io.Source.fromFile(file)(encoding)
      }catch{
        case x:RuntimeException => (x.printStackTrace()); println("Cannot checkout. File does not exist"); sys.exit();
        case x:FileNotFoundException =>(x.printStackTrace()); println("Cannot checkout. File does not exist"); sys.exit();
        case x:FileSystemNotFoundException =>(x.printStackTrace()); println("Cannot checkout. File does not exist"); sys.exit();
      }
    }
      var original = txtFile.getLines.toList
      if(original.nonEmpty && !original.head.isEmpty && original.head.charAt(0) == '\uFEFF')
        original = original.head.substring(1) :: original.tail
      val fixedExpressions = fixMultiLineExpression(original)
      //txtFile.close
      val sourceString = fixedExpressions.mkString("\n")
      sourceString
  }
  def normalizeFileFromString(file: String) = {
    var original = file.split("\n").toList
    if(original.nonEmpty && !original.head.isEmpty && original.head.charAt(0) == '\uFEFF')
      original = original.head.substring(1) :: original.tail
    val fixedExpressions = fixMultiLineExpression(original)
    val sourceString = fixedExpressions.mkString("\n")
    sourceString
  }

  /**
   * Normalizes the multi-line expressions in preprocessor directives
   * @param file
   * @return
   */
  def fixMultiLineExpression(file: List[String]): List[String] = {

    @tailrec
    def run(input: List[String], acc: List[String]): List[String] = {
      var exp = ""
      input match {
        case h :: t if (ifRegex.reset(h).matches() || elifRegex.reset(h).matches()) => {
          if (h.endsWith("\\")) {
            exp = h.replace("\\", " ")
            var tail = t
            while (tail.head.endsWith("\\")) {
              exp = exp + tail.head.replace("\\", " ")
              tail = tail.tail
            }
            exp = exp + tail.head
            run(tail.tail, exp:: acc)
          } else
            run(t, h:: acc)
        }
        case h :: t => {
          run(t, h:: acc)
        }
        case List(x) => (x :: acc).reverse
        case List() => acc.reverse
      }
    }
    run(file, List())
  }


}