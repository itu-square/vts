package dk.itu.vts.util

import java.io.File
import java.nio.file.Files
import java.util.regex.Pattern

import dk.itu.vts.commands.Storage

import scala.language.implicitConversions

import scala.sys.process.{Process, ProcessLogger}

/**
  * Created by scas on 12-09-2015.
  */
trait FileUtil extends Storage{
  val whitespace = Pattern.compile( """\s*|\n""").matcher("")

  def run(in: String, folder: String): (List[String], List[String], Int) = {
    var out = List[String]()
    var err = List[String]()
    val process = Process(in, new File(folder))
    val exit = process ! ProcessLogger((s) => out ::= s, (s) => err ::= s)
    (out.reverse, err.reverse, exit)
  }

  def printToFile(f: java.io.File)(op: java.io.PrintWriter => Unit) {
    val p = new java.io.PrintWriter(f)
    try {
      op(p)
    } finally {
      p.flush()
      p.close()
    }
  }

  def readFromFileToString(path: String)(removeEmptyLines: Boolean = false): String = {
    val str = io.Source.fromFile(path).mkString
    if (removeEmptyLines) str.replaceAll( """(?m)^(\+|-)?[ \t]*\r?\n""", "")
    else str
  }

  def readFromFileToList(path: String)(removeEmptyLines: Boolean = false): List[String] = {
    val file = io.Source.fromFile(path).getLines.toList
    if (removeEmptyLines)
      file.filterNot(x => (x.isEmpty || whitespace.reset(x).matches))
    else
      file
  }

  def findEmptyDirectories(dir: String) = {
    val dir_files = new File(dir).listFiles
    val result = collection.mutable.MutableList[String]()

    def search(files: Array[File]): Unit = {
      for (f <- files) {
        if (f.isDirectory) {
          if(f.listFiles.isEmpty)
            result += f.getAbsolutePath
          else
            search(f.listFiles)
        }
      }
    }
    search(dir_files)
    result.toList
  }

  /**
    * Return a list of the files in the directory and all its subfolders <=> recursive is set
    * Filter the type of the files desired.
    * @param dir
    * @param recursive
    * @param filter
    * @return
    */
  def files(dir: String, recursive: Boolean, filter: List[String]): List[String] = {
    val dir_files = new File(dir).listFiles
    val result = collection.mutable.MutableList[String]()

    def search(files: Array[File]): Unit = {
      for (f <- files) {

        if (f.isFile && filter.contains(extension(f.getAbsolutePath))) {
          result += (f.getAbsolutePath)
        }
        else if (f.isDirectory && recursive)
          (search(f.listFiles()))
        else if (!recursive && filter.contains(extension(f.getAbsolutePath)))
          result += (f.getAbsolutePath)
      }
    }

    search(dir_files)
    result.toList
  }

  def filesRelativePath(dir: String, recursive: Boolean, filter: List[String]): List[String] = {
    val dir_files = new File(dir).listFiles
    val result = collection.mutable.MutableList[String]()

    def search(files: Array[File]): Unit = {
      for (f <- files) {

        if (f.isFile && filter.contains(extension(f.getPath))) {
          result += (f.getPath).substring(2)
        }
        else if (f.isDirectory && recursive && !f.getPath.equals(VTSFolder))
          (search(f.listFiles()))
        else if (!recursive && filter.contains(extension(f.getPath)))
          result += (f.getPath).substring(2)
      }
    }
    search(dir_files)
    result.toList
  }

  def extension(file: String) = {
    var extension = ""
    val i = file.lastIndexOf('.')
    if (i > 0) {
      extension = file.substring(i + 1)
    }
    extension
  }

  class StringSplitToTuple(s: String) {
    def splitToTuple(regex: String): (String, String) = {
      s.split(regex) match {
        case Array(str1, str2) => (str1, str2)
        case Array(str1) => (str1, "")
        case _ => sys.error("too many colons")
      }
    }
  }

  object SplitToTuple {
    implicit def splitToTuple(regex: String) = new StringSplitToTuple(regex)
  }

}