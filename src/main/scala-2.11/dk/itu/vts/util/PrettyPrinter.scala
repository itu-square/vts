package dk.itu.vts.util

import dk.itu.vts.expression.Expression
import dk.itu.vts.model.ProgamAST._
import dk.itu.vts.model.{Choiced, NoChoice, YourChoice}

import scala.language.higherKinds
/**
  * Created by scas on 05-04-2016.
  */
object PrettyPrinter {

  def prettyPrint[T[_]](ast: Program[T]): List[String] = {
    def printChoice(choice: YourChoice[T], elif: Boolean): List[String] = elif match{
      case true => List(List("#elif " + choice.expr.toString), printLeftChoices(choice.left), printRightChoices(choice.right,choice.expr)).flatten
      case _ if (choice.right.isDefined) => List(List("#if " + choice.expr.toString), printLeftChoices(choice.left), printRightChoices(choice.right, choice.expr), List("#endif //" + choice.expr)).flatten
      case _ if !(choice.right.isDefined) => List(List("#if " + choice.expr.toString), printLeftChoices(choice.left), List("#endif //" + choice.expr)).flatten
    }

    def printLeftChoices(s: Seq[Choiced[T]]): List[String] = s match{
      case h +: t => h match {
        case x: NoChoice[T] => List(x.toString) ++ prettyPrint(t)
        case x: YourChoice[T] => printChoice(x, false) ++ prettyPrint(t)
      }
      case Seq(e) => e match {
        case x: NoChoice[T] => List(x.toString)
        case x: YourChoice[T] => printChoice(x, false)
      }
      case Seq() => Nil
    }
    def printRightChoices(s: Option[Seq[Choiced[T]]],expr: Expression): List[String] = s match {
      case Some(h +: t) => h match {
        case x: YourChoice[T] if t.isEmpty =>  List("#else") ++ printChoice(x,false) ++ prettyPrint(t)
        case x: YourChoice[T] if !t.isEmpty && !t.head.isInstanceOf[YourChoice[T]] => List("#else") ++ printChoice(x,false) ++ prettyPrint(t)
        case x: YourChoice[T] => printChoice(x,true) ++ prettyPrint(t)
        case x: NoChoice[T] => List("#else //"+expr.toString, x.toString) ++ prettyPrint(t)
      }
      case Some(x: Seq[Choiced[T]]) => prettyPrint(x)
      case Some(Seq()) => Nil
      case None => Nil
    }
    printLeftChoices(ast)
  }
}