package dk.itu.vts.util

import dk.itu.vts.expression.Expression
import dk.itu.vts.sat.{IdMap, SATBuilder}
import org.sat4j.specs.ContradictionException

/**
  * Created by scas on 25-10-2015.
  */
trait SatCheck {

  val boolExprs1 = List("And", "Or", "Negation", "Identifier", "Defined")
  val boolExprs2 = List("QuestionMark", "IntLiteral", "StringLiteral", "FloatLiteral", "CharacterLiteral", "HexLiteral",
    "OctalLiteral", "DecimalLiteral", "BitwiseAnd", "BitwiseOr", "BitwiseXor", "BitwiseNegation", "LessThan", "LessThanOrEquals",
    "GreaterThan", "GreaterThanOrEquals", "EqualsTo", "NotEqualsTo", "Times", "Divide", "Plus", "Minus", "Mod",
    "ShiftLeft", "ShiftRight", "UnaryMinus", "UnaryPlus", "BIff", "FunctionCall", "Implies", "TempBinaryOp"
  )

  import org.kiama.rewriting.Rewriter._

  private def isBooleanExpr(astPC: Expression) = {
    val exprs = collectl {
      case ex: Expression => ex.getClass.getTypeName.replace("dk.itu.vcs.dk.itu.vts.model.", "")
    }(astPC)

    exprs.isEmpty || exprs.intersect(boolExprs2).size == 0
  }

  def checkSat(astPC: Expression, projection: Expression): Boolean = {
    val cnfPC = astPC.toCNF
    val cnfProjection = projection.toCNF
    val idMapPC = IdMap(List(cnfPC))
    val idMapChoice = IdMap(List(cnfProjection))

    try {
      SATBuilder.apply(List(cnfPC, cnfProjection), (idMapPC ++ idMapChoice).keys.toSet).isSatisfiable
    }
    catch {
      case e: ContradictionException if (e.getMessage.equals("Creating Empty clause ?")) => false
    }
  }
}