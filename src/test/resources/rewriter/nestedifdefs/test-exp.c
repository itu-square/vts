#if defined(CHDK)
#if VOXEL_CLONE
    line
    if (chdkActive)
#else
    if (chdkActive && (millis() - chdkHigh > CHDK_DELAY))
#endif
    {
      chdkActive = false;
#if VOXEL_CLONE
      if (millis()-chdkHigh < CHDK_DELAY) return;
#endif
      WRITE(CHDK, LOW);
    }
#endif //defined(CHDK)
