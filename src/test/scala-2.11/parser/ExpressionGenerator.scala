package parser

import dk.itu.vts.expression._
import org.scalacheck.Gen._
import org.scalacheck._

/**
 * Created by scas on 12-09-2015.
 */
trait ExpressionGenerator {

  val genId : Gen[Identifier] =
    for (a <- alphaChar; b <- alphaChar)
      yield Identifier("" + a + b)

  val genDefined: Gen[Defined] =
    for (a <- alphaChar; b <- alphaChar)
      yield Defined("" + a + b)

  def genAnd(i : Int) : Gen[And] =
    for (l <- genExpr(i); r <- genExpr(i))
      yield And(l, r)

  def genImplies(i : Int) : Gen[Implies] =
    for (l <- genExpr(i); r <- genExpr(i))
      yield Implies(l, r)

  def genOr(i : Int) : Gen[Or] =
    for (l <- genExpr(i); r <- genExpr(i))
      yield Or(l, r)

  def genGreaterThan(i: Int) : Gen[GreaterThan] =
    for( l<- genExpr(i); r <- genIntLiteral(i))
      yield GreaterThan(l,r)

  def genLessThan(i: Int) : Gen[LessThan] =
    for( l<- genExpr(i); r <- genIntLiteral(i))
      yield LessThan(l,r)

  def genGreaterThanOrEquals(i: Int) : Gen[GreaterThanOrEquals] =
    for( l<- genExpr(i); r <- genIntLiteral(i))
      yield GreaterThanOrEquals(l,r)

  def genLessThanOrEquals(i: Int) : Gen[LessThanOrEquals] =
    for( l<- genExpr(i); r <- genIntLiteral(i))
      yield LessThanOrEquals(l,r)

  def genIntLiteral(i: Int) : Gen[IntLiteral] =
    for (expr <- genExpr(i))
      yield IntLiteral(i)

  def genTimes(i: Int) : Gen[Times] =
    for( l<- genExpr(i); r <- genIntLiteral(i))
      yield Times(l,r)

  def genDivide(i: Int) : Gen[Divide] =
    for( l<- genExpr(i); r <- genIntLiteral(i))
      yield Divide(l,r)

  def genNot(i : Int) : Gen[Negation] =
    for (expr <- genExpr(i))
      yield Negation(expr)


  def genOp(i : Int) =
    oneOf( genAnd(i), genOr(i), genIntLiteral(i),  genLessThanOrEquals(i), genLessThanOrEquals(i), genGreaterThan(i), genGreaterThanOrEquals(i), genTimes(i), genDivide(i))

  implicit def arbExpr : Arbitrary[Expression] = Arbitrary(sized { s => genExpr(s) })

  def genExpr(i : Int) : Gen[Expression] =
    if (i == 0) genId else genOp(i/2)

//  val genError : Gen[Error] =
//    for (s <- identifier)
//      yield Error

//  val genWarning : Gen[Warning] =
//    for (s <- identifier)
//      yield Warning(s)


}