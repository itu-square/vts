package parser

import dk.itu.vts.expression._
import dk.itu.vts.expression.ExpressionParserBoolean
import org.scalatest.{FunSuite, Matchers}

/**
  * Created by scas on 15-02-2016.
  */
class ExpressionParserBooleanTest extends FunSuite with Matchers {

  test("Or ") {
    ExpressionParserBoolean.parse("#if A || B").get shouldBe Or(Identifier("A"),Identifier("B"))
    ExpressionParserBoolean.parse("#if A || defined(B)").get shouldBe Or(Identifier("A"),Defined("B"))
    ExpressionParserBoolean.parse("#if (A && C) || (A || B)").get shouldBe Or(And(Identifier("A"),Identifier("C")),Or(Identifier("A"),Identifier("B")))
  }

  test("And"){
    ExpressionParserBoolean.parse("#if A && B").get shouldBe And(Identifier("A"),Identifier("B"))
    ExpressionParserBoolean.parse("#if !A && B").get shouldBe And(Negation(Identifier("A")),Identifier("B"))
    ExpressionParserBoolean.parse("#if A && !B").get shouldBe And(Identifier("A"),Negation(Identifier("B")))
    ExpressionParserBoolean.parse("#if !A && !B").get shouldBe And(Negation(Identifier("A")),Negation(Identifier("B")))
    ExpressionParserBoolean.parse("#if !(A && B)").get shouldBe Negation(And(Identifier("A"),Identifier("B")))
    ExpressionParserBoolean.parse("#if (A || B) && C").get shouldBe And(Or(Identifier("A"),Identifier("B")), Identifier("C"))
  }

  test("Defined"){
    ExpressionParserBoolean.parse("#ifdef A").get shouldBe Defined("A")

    ExpressionParserBoolean.parse("#if defined(A)").get shouldBe Defined("A")
    ExpressionParserBoolean.parse("#if defined A").get shouldBe Defined("A")
    ExpressionParserBoolean.parse("#if not defined(A)").get shouldBe Negation(Defined("A"))
    ExpressionParserBoolean.parse("#if !defined(A)").get shouldBe Negation(Defined("A"))
    ExpressionParserBoolean.parse("#ifdef !A").get shouldBe Negation(Defined("A"))
    ExpressionParserBoolean.parse("#ifdef !(A)").get shouldBe Negation(Defined("A"))
    ExpressionParserBoolean.parse("#ifndef !A").get shouldBe (Defined("A"))
    ExpressionParserBoolean.parse("#ifndef A").get shouldBe Negation(Defined("A"))
    ExpressionParserBoolean.parse("#ifdef (A)").get shouldBe Defined("A")
  }

  test("Identifier"){
    ExpressionParserBoolean.parse("#if (A)").get shouldBe Identifier("A")
    ExpressionParserBoolean.parse("#if A").get shouldBe Identifier("A")
    ExpressionParserBoolean.parse("#if !A").get shouldBe Negation(Identifier("A"))
    ExpressionParserBoolean.parse("#if   A").get shouldBe (Identifier("A"))
  }

  test("True"){
    ExpressionParserBoolean.parse("#if 1").get shouldBe True()
    ExpressionParserBoolean.parse("#elif 1").get shouldBe True()
    ExpressionParserBoolean.parse("#if  1 ").get shouldBe True()
    ExpressionParserBoolean.parse("#elif  1 ").get shouldBe True()
  }

  test("False"){
    ExpressionParserBoolean.parse("#if 0").get shouldBe False()
    ExpressionParserBoolean.parse("#elif 0").get shouldBe False()
    ExpressionParserBoolean.parse("#if  0 ").get shouldBe False()
    ExpressionParserBoolean.parse("#elif  0 ").get shouldBe False()
  }

  test("Random expression"){
    println(ExpressionParserBoolean.parse("#if 1").get.getClass)
  }


}