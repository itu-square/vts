package parser

import dk.itu.vts.expression._
import org.scalacheck.Prop.forAll
import org.scalatest.{Matchers, FunSuite}
import org.scalatest.prop.Checkers

/**
 * Created by scas on 27-08-2015.
 */
class ExpressionParserTest extends FunSuite with Checkers with ExpressionGenerator with Matchers{

  test("Parsing multiple ifdefs"){
    val expr2 = "#if A + B * C"
    val exp2 = Plus(Identifier("A"), Times(Identifier("B"), Identifier("C")))
    assertResult(exp2)( ExpressionParser.parse(expr2).get)

    val expr3 = "#if A || B && C"
    val exp3 = Or(Identifier("A"), And(Identifier("B"), Identifier("C")))
    assertResult(exp3)(ExpressionParser.parse(expr3).get)

    val expr4 = "#if A & B && C"
    val exp4 = And(BitwiseAnd(Identifier("A"),  Identifier("B")), Identifier("C"))
    assertResult(exp4)(ExpressionParser.parse(expr4).get)

    val expr5 = "#if A & B == C"
    val exp5 = BitwiseAnd(Identifier("A"), EqualsTo(Identifier("B"), Identifier("C")))
    assertResult(exp5)(ExpressionParser.parse(expr5).get)

    val expr6 = "#if A & B + C"
    val exp6 = BitwiseAnd(Identifier("A"), Plus(Identifier("B"), Identifier("C")))
    assertResult(exp6)(ExpressionParser.parse(expr6).get)

    val expr7 = "#if A << B & C"
    val exp7 = BitwiseAnd(ShiftLeft(Identifier("A"), Identifier("B")), Identifier("C"))
    assertResult(exp7)(ExpressionParser.parse(expr7).get)
  }

  test("Parse true") {
    ExpressionParser.parse("1").getOrElse(fail()) shouldBe True()
    ExpressionParser.parse("true").getOrElse(fail()) shouldBe True()
  }

  test("Parse false") {
    ExpressionParser.parse("0") shouldBe True()
    ExpressionParser.parse("false") shouldBe True()
  }

  test("Parsing ifexpr") {
    val exp = "#if A"
    val r = ExpressionParser.parse(exp).get
    assertResult(Identifier("A"))(r)
  }

  test("Parsing negated if expr") {
    val exp = "#if !A"
    val r = ExpressionParser.parse(exp).get
    assertResult(Negation(Identifier("A")))(r)
  }

  test("Parsing ifdef expr") {
    val exp = "#ifdef A"
    val r = ExpressionParser.parse(exp).get
    assertResult(Defined("A"))(r)
  }
  test("Parsing negated ifdef expr") {
    val exp = "#ifdef !A"
    val r = ExpressionParser.parse(exp).get
    assertResult(Negation(Defined("A")))(r)
  }
  test("Parsing ifndef expr") {
    val exp = "#ifndef A"
    val r = ExpressionParser.parse(exp).get
    assertResult(Negation(Defined("A")))(r)
  }

  test("Parsing Or expr") {
    val exp = "A || B"
    val nested_exp = "A || (B || C)"
    assertResult(Or(Identifier("A"), Identifier("B")))(ExpressionParser.parse(exp).get)
    assertResult(Or(Identifier("A"), Or(Identifier("B"), Identifier("C"))))(ExpressionParser.parse(nested_exp).get)
  }
  test("Parsing expressions") {
    val exp = "#if A > 2"
    assertResult(GreaterThan(Identifier("A"), IntLiteral(2)))(ExpressionParser.parse(exp).get)
  }

  test("Parsing marlin expr") {
    val exp = """#if defined(ENABLE_AUTO_BED_LEVELING) && (defined(SERVO_ENDSTOPS) || defined(Z_PROBE_ALLEN_KEY)) && !defined(Z_PROBE_SLED)"""
    val parsed = ExpressionParser.parse(exp).get
    val expected = And(Defined("ENABLE_AUTO_BED_LEVELING"), And(Or(Defined("SERVO_ENDSTOPS"), Defined("Z_PROBE_ALLEN_KEY")), Negation(Defined("Z_PROBE_SLED"))))
    assertResult(expected)(parsed)
  }

  test("Parsing if defined"){
    val exp = "#if defined(A)"
    assertResult(Defined("A"))(ExpressionParser.parse(exp).get)
  }
  test("Parsing and defined expr"){
    val exp = "#if defined(A) && defined(B)"
    assertResult(And(Defined("A"), Defined("B")))(ExpressionParser.parse(exp).get)
  }
  test("Parsing or defined expr"){
    val exp = "#if defined(A) || defined(B)"
    assertResult(Or(Defined("A"), Defined("B")))(ExpressionParser.parse(exp).get)
  }
  test("Parsing or not defined expr"){
    val exp = "#if !defined(A) || defined(B)"
    assertResult(Or(Negation(Defined("A")), Defined("B")))(ExpressionParser.parse(exp).get)
  }
  test("Parsing not defined expr"){
    val exp = "#if not defined(A)"
    assertResult(Negation(Defined("A")))(ExpressionParser.parse(exp).get)
  }
  test("Parsing octal literal in expression"){
    val exp = """#if F_CPU == 16000000UL"""
    val expected = EqualsTo(Identifier("F_CPU"), OctalLiteral("16000000UL"))
    assertResult(expected)(ExpressionParser.parse(exp).get)
  }
  test("Parsing greater than"){
    val exp = "#if A > 2"
    assertResult(GreaterThan(Identifier("A"),IntLiteral(2)))(ExpressionParser.parse(exp).get)
  }

  test("Parsing greater than or equals"){
    val exp = "A >= 2"
    assertResult(GreaterThanOrEquals(Identifier("A"),IntLiteral(2)))(ExpressionParser.parse(exp).get)
  }
  test("Parsing less than"){
    val exp = "#if A < 2"
    assertResult(LessThan(Identifier("A"),IntLiteral(2)))(ExpressionParser.parse(exp).get)
  }
  test("Parsing less than or equals"){
    val exp = "#if A <= 2"
    assertResult(LessThanOrEquals(Identifier("A"),IntLiteral(2)))(ExpressionParser.parse(exp).get)
  }

  test("Parsing int literals"){
    val exp = "2"
    assertResult(IntLiteral(2))(ExpressionParser.parse(exp).get)
    assertResult(IntLiteral(1))(ExpressionParser.parse("#if 1").get)
  }

  test("Parsing float literals"){
    val exp = "#if A > 2.4"
    assertResult(GreaterThan(Identifier("A"),FloatLiteral("2.4")))(ExpressionParser.parse(exp).get)
  }
  test("Parsing negative int literals"){
    val exp = "-2"
    assertResult(UnaryMinus(IntLiteral(2)))(ExpressionParser.parse(exp).get)
  }

  test("Parsing ENABLED macro"){
    val exp = "#if ENABLED ( AUTO_BED_LEVELING )"
    val exp1 = "#if ENABLED AUTO_BED_LEVELING"
    assertResult(Identifier("AUTO_BED_LEVELING"))(ExpressionParser.parse(exp).get)
    assertResult(Identifier("AUTO_BED_LEVELING"))(ExpressionParser.parse(exp1).get)
  }
  test("Parsing DISABLED macro"){
    val exp = "#if DISABLED AUTO_BED_LEVELING "
    val exp1 = "#if DISABLED(AUTO_BED_LEVELING)"
    assertResult(Negation(Identifier("AUTO_BED_LEVELING")))(ExpressionParser.parse(exp).get)
    assertResult(Negation(Identifier("AUTO_BED_LEVELING")))(ExpressionParser.parse(exp1).get)
  }

  test("Parsing random expr") {
    val exp = """#if 1 && !defined(BLINKM)"""
    val expected = And(True(), Negation(Defined("BLINKM")))
    val parsedExpr = ExpressionParser.parse(exp).get
    println(parsedExpr.asInstanceOf[And].left.getClass)
    assertResult(expected)(parsedExpr)
  }

  test("Checker"){
    val expressions = forAll {exp: Expression => println(exp); ExpressionParser.parse(exp.toString).get == exp}
    check { (e : Expression) => println("Processing: " + e); expressions}
  }

}