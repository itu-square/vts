package parser

import dk.itu.vts.util.FileNormalizer
import org.scalatest.FunSuite

/**
 * Created by scas on 14-10-2015.
 */
class FileNormalizerTest extends FunSuite{

  test("Testing the normalizer"){
    val src = "test-files/parser/sdpinmap.h"
    val expected = "test-files/parser/sdpinmap_normalized.h"

    assertResult(io.Source.fromFile(expected).getLines.mkString("\n"))(FileNormalizer.normalizeFile(src, "ISO-8859-1"))

  }

}