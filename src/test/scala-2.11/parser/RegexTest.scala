package parser

import org.scalatest.FunSuite

/**
 * Created by scas on 27-08-2015.
 */
class RegexTest extends FunSuite {

  test("Regex test") {


    val ifdef = """[ \t]*\#ifdef.*"""
    val if_ = """[ \t]*\#if[^def].*"""
    val ifndef = "[ \\t]*\\#ifndef.*"
    val elif = "[ \\t]*\\#elif.*"
    val endif = """[ \t]*\#endif.*"""
    val else_ = """[ \t]*\#else.*"""



    assertResult(true)("#ifdef test".matches(ifdef))
    assertResult(true)("  #ifdef test".matches(ifdef))
    assertResult(true)("#ifdef A && B // comment".matches(ifdef))
    assertResult(false)("a #ifdef test".matches(ifdef))
    assertResult(false)("/#ifdef test".matches(ifdef))
    assertResult(true)("#ifndef test".matches(ifndef))

    assertResult(true)("#ifdef test".matches(ifdef))
    assertResult(true)("#ifdef test".matches(ifdef))
  }

  test("Regex test1") {


    val ifdef = """\s*\#ifdef\s*(.*)""".r
    val if_ = """[ \t]*\#if[^def].*""".r
    val ifndef = "[ \\t]*\\#ifndef.*".r
    val elif = "[ \\t]*\\#elif.*".r
    val endif = """[ \t]*\#endif.*""".r
    val else_ = """[ \t]*\#else.*""".r

    "#ifdef A" match {
      case ifdef(x) => println(s"Found ifdef $x")
      case ifndef(x) => println("Found ifndef")
      case _ => println("found textline")
    }


  }

  test("Removing comments from expressions"){
    val str = """#if A // a /* oare */"""
    val strToBeParsed = str.split("//")(0)
    val finalString = strToBeParsed.replaceAll("/\\*.*\\*/", "")
    assertResult("#if A ")(finalString)
  }


}