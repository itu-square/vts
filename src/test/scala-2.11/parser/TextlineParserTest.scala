package parser

import java.io.File

import dk.itu.vts.model.{Textline, Choiced}
import dk.itu.vts.parser.Parser
import dk.itu.vts.util.FileUtil
import org.scalatest.FunSuite

/**
 * Created by scas on 23-09-2015.
 */
class TextlineParserTest extends FunSuite with FileUtil {

  case class ParsedFile(file: String, result: Option[Seq[Choiced[Textline]]], error: String)

  def parseFiles(folder: String): List[ParsedFile] = {
    val f = files(folder, true, List("c", "h", "cpp"))
    val result = collection.mutable.MutableList[ParsedFile]()
    for (i <- f){
      print("Parsing " + i )
      Parser.parseFile(i) match {
        case Left(e) => {print(".... parsing failed\n" ); result+=ParsedFile(i, None, e)}
        case Right(e) => result+=ParsedFile(i, Some(e), ""); print("\n")
      }}
  result.toList
  }
  test("Parse random strings"){
    println(Parser.parseString(
      """#if BB_BIG_ENDIAN
        |	}
        |#endif
        |""".stripMargin))

  }
  test("Parse random files") {
    val file = "evaluation/Marlin/patches/d3fe3a0/Marlin/cardreader.h"
    Parser.parseFile(file) match {
      case Left(e) => {
        print(e)
      }
      case Right(e) => println(e)
    }
  }

  test("Parse comment with #if directives in it"){
    val src = "test-files/parser/comments.txt"
    val result = Parser.parseFile(src)
    println(result)
  }

  test("Parse all files"){
    val f = new File("test-files/parser").listFiles()
    for (i <- f){println("Parsing " + i.getPath + "\n"); Parser.parseFile(i.getPath)}
  }

  test("Parse all busybox files"){
    val r = parseFiles("busybox")
    val p = r.filter(x => x.result.isEmpty)
    p.foreach(println)
    println("All: " + r.size + "; failed: " + p.size)
  }
  test("Parse all marlin files"){
    val r = parseFiles("Marlin/Marlin")
    val p = r.filter(x => x.result.isEmpty)
    println("All: " + r.size + "; failed: " + p.size)
  }
  test("Parse all mysql files"){
    val r = parseFiles("mysql-5.7.9/")
    val p = r.filter(x => x.result.isEmpty)
    println("All: " + r.size + "; failed: " + p.size)
    p.foreach(println)
  }
  test("Parse all linux files"){
    //val f = files("C:/ITU/Repositories/linux", true, List("cpp", "h", "c"))
    //for (i <- f){println("Parsing " + i + "\n"); Parser.parseFile(i)}
    val r = parseFiles("C:/ITU/Repositories/linux")
    val p = r.filter(x => x.result.isEmpty)
    println("All: " + r.size + "; failed: " + p.size)
  }
  test("Parse all git files"){
    val f = files("C:/ITU/repositories/Repetier-Firmware\\src\\ArduinoAVR\\Repetier", true, List("cpp", "h"))
    for (i <- f){println("Parsing " + i + "\n"); Parser.parseFile(i)}
  }

}