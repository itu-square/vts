package rewriter

import dk.itu.vts.editing.Checkin
import dk.itu.vts.expression._
import dk.itu.vts.model.{YourChoice, Choiced, Textline, NoChoice}
import dk.itu.vts.parser.Parser
import dk.itu.vts.rewriter.SourceRules._
import dk.itu.vts.util.PrettyPrinter
import org.scalatest.{Matchers, FunSuite}
import scala.io.Source

/**
  * Created by scas on 08-05-2016.
  */
class RewriterTest extends FunSuite with Matchers{

  test("Rewrite tree that modified first line and last line of text"){
    val origSrc = io.Source.fromURL(getClass.getResource("/rewriter/rw1.scala")).mkString
    val updatedSrc = io.Source.fromURL(getClass.getResource("/rewriter/rw1-edit.scala")).mkString
    val origAST = Parser.parseString(origSrc).right.get
    val updatedAST = Parser.parseString(updatedSrc).right.get
    val result = Checkin.checkinWithNesting(True(),Identifier("CONFIG_STATUS"), origAST,updatedAST)
    //val rewriteResult = SourceRules.rewriteAST(result)
    PrettyPrinter.prettyPrint(result).foreach(println)
    //PrettyPrinter.prettyPrint(rewriteResult).foreach(println)
  }


  test("Transform to list"){

    val src = Seq(NoChoice[Textline](Textline("test")), Seq(NoChoice[Textline](Textline("lala"))))
    val exp = List(NoChoice[Textline](Textline("test")), List(NoChoice[Textline](Textline("lala"))))
    assertResult(exp)(rewrite(transformToList)(src))

  }


  test("Factoring"){
    val expr = Identifier("A")
    val src1 = List(YourChoice(expr,List(NoChoice[Textline](Textline("if_branch1"))),Some(List(NoChoice[Textline](Textline("else_branch"))))),YourChoice(expr,List(NoChoice[Textline](Textline("if_branch2"))),Some(List(NoChoice[Textline](Textline("else_branch2"))))),List())
    val exp1 = List(YourChoice(expr,List(NoChoice[Textline](Textline("if_branch1")),NoChoice[Textline](Textline("if_branch2"))),Some(List(NoChoice[Textline](Textline("else_branch")),NoChoice[Textline](Textline("else_branch2"))))),List())
    assertResult(exp1)(rewrite(factoring)(src1))

    // Negative expr
    val src2 = List(YourChoice(expr,List(NoChoice[Textline](Textline("if_branch1"))),Some(List(NoChoice[Textline](Textline("else_branch"))))),YourChoice(Negation(expr),List(NoChoice[Textline](Textline("if_branch2"))),Some(List(NoChoice[Textline](Textline("else_branch2"))))),List())
    val exp2 = List(YourChoice(expr,List(NoChoice[Textline](Textline("if_branch1")),NoChoice[Textline](Textline("else_branch2"))),Some(List(NoChoice[Textline](Textline("else_branch")),NoChoice[Textline](Textline("if_branch2"))))),List())
    assertResult(exp2)(rewrite(factoring)(src2))
  }

  test("ASTFactIdempotency"){

    //case YourChoice(expr: Expression, left: Program[T], Some(right: Program[T])) +: t if left.nonEmpty && right.nonEmpty && (left.head == right.head) => Seq(left.head, YourChoice(expr, (left.tail), Some((right.tail)))) ++ t
    // First case -  pos
    val expr = Identifier("A")
    val src = YourChoice(expr, List(NoChoice[Textline](Textline("test")),NoChoice[Textline](Textline("if_branch"))),Some(List(NoChoice[Textline](Textline("test")),NoChoice[Textline](Textline("elsebranch"))))) :: List()
    val exp = NoChoice[Textline](Textline("test")) :: YourChoice(expr, List(NoChoice[Textline](Textline("if_branch"))),Some(List(NoChoice[Textline](Textline("elsebranch"))))) :: List()
    assertResult(exp)(rewrite(ASTFactIdempotency)(src))
    // First case -  neg
    val src1 = YourChoice(expr, List(NoChoice[Textline](Textline("test")),NoChoice[Textline](Textline("if_branch"))),Some(List(NoChoice[Textline](Textline("elsebranch"))))) :: List()
    assertResult(src1)(rewrite(ASTFactIdempotency)(src1))
    val src2 = YourChoice(expr, List(NoChoice[Textline](Textline("test")),NoChoice[Textline](Textline("if_branch"))),Some(List())) :: List()
    assertResult(src2)(rewrite(ASTFactIdempotency)(src2))


    // Second case -  pos
    val src3 = YourChoice(expr, List(NoChoice[Textline](Textline("test1")),NoChoice[Textline](Textline("if_branch"))),Some(List(NoChoice[Textline](Textline("test")),NoChoice[Textline](Textline("if_branch"))))) :: List()
    val exp3 = YourChoice(expr, List(NoChoice[Textline](Textline("test1"))),Some(List(NoChoice[Textline](Textline("test"))))) :: NoChoice[Textline](Textline("if_branch")) :: List()
    assertResult(exp3)(rewrite(ASTFactIdempotency)(src3))
    // Second case -  neg
    val src4 = YourChoice(expr, List(NoChoice[Textline](Textline("test")),NoChoice[Textline](Textline("if_branch"))),Some(List(NoChoice[Textline](Textline("elsebranch"))))) :: List()
    assertResult(src4)(rewrite(ASTFactIdempotency)(src4))
    val src5 = YourChoice(expr, List(NoChoice[Textline](Textline("test")),NoChoice[Textline](Textline("if_branch"))),Some(List())) :: List()
    assertResult(src5)(rewrite(ASTFactIdempotency)(src5))

    // Third case -  pos
    val src6 = Seq(YourChoice(expr, List(NoChoice[Textline](Textline("test")),NoChoice[Textline](Textline("if_branch"))),Some(List(NoChoice[Textline](Textline("test")),NoChoice[Textline](Textline("else_branch"))))))
    val exp6 = Seq(NoChoice[Textline](Textline("test")),YourChoice(expr, List(NoChoice[Textline](Textline("if_branch"))),Some(List(NoChoice[Textline](Textline("else_branch"))))))
    assertResult(exp6)(rewrite(ASTFactIdempotency)(src6))

    // Fourth case -  pos
    val src7 = Seq(YourChoice(expr, List(NoChoice[Textline](Textline("test1")),NoChoice[Textline](Textline("if_branch"))),Some(List(NoChoice[Textline](Textline("test")),NoChoice[Textline](Textline("if_branch"))))))
    val exp7 = Seq(YourChoice(expr, List(NoChoice[Textline](Textline("test1"))),Some(List(NoChoice[Textline](Textline("test"))))),NoChoice[Textline](Textline("if_branch")))
    assertResult(exp7)(rewrite(ASTFactIdempotency)(src7))
  }

  test("Idempotency"){
    val expr = Identifier("A")
    val src1 = YourChoice(expr, List(NoChoice[Textline](Textline("test")),NoChoice[Textline](Textline("if_branch"))),Some(List(NoChoice[Textline](Textline("test")),NoChoice[Textline](Textline("if_branch"))))) :: List()
    val exp1 = List(NoChoice[Textline](Textline("test")), NoChoice[Textline](Textline("if_branch")))
    assertResult(exp1)(rewrite(idempotency)(src1))
    // Neg
    val src2 = YourChoice(expr, List(NoChoice[Textline](Textline("test"))),Some(List(NoChoice[Textline](Textline("test")),NoChoice[Textline](Textline("if_branch"))))) :: List()
    assertResult(src2)(rewrite(idempotency)(src2))

    val src3 = Seq(YourChoice(expr, List(),Some(List())))
    val exp3 = List()
    assertResult(exp3)(rewrite(idempotency)(src3))
  }

  test("Flip negation"){
    val expr = Identifier("A")
    val src1 = YourChoice(expr, List(NoChoice[Textline](Textline("test"))),None)
    val exp1 = YourChoice(expr, List(NoChoice[Textline](Textline("test"))),None)
    assertResult(exp1)(rewrite(flipNegation)(src1))

    val src2 = YourChoice(Negation(expr), List(NoChoice[Textline](Textline("test"))),None)
    val exp2 = YourChoice(Negation(expr), List(NoChoice[Textline](Textline("test"))),None)
    assertResult(exp2)(rewrite(flipNegation)(src2))

    val src3 = YourChoice(Negation(expr), List(),Some(List(NoChoice[Textline](Textline("test")))))
    val exp3 = YourChoice(expr, List(NoChoice[Textline](Textline("test"))),None)
    assertResult(exp3)(rewrite(flipNegation)(src3))

    val src4 = YourChoice(Negation(expr), List(NoChoice[Textline](Textline("hello"))),Some(List(NoChoice[Textline](Textline("test")))))
    val exp4 = YourChoice(expr,List(NoChoice[Textline](Textline("test"))) ,Some(List(NoChoice[Textline](Textline("hello")))))
    assertResult(exp4)(rewrite(flipNegation)(src4))

    val src5 = YourChoice(Negation(expr), List(NoChoice[Textline](Textline("hello"))),Some(List()))
    assertResult(src5)(rewrite(flipNegation)(src5))

    val src6 = YourChoice((expr), List(), Some(List()))
    assertResult(src6)(rewrite(flipNegation)(src6))

    val src7 = YourChoice((expr), List(), Some(List(NoChoice[Textline](Textline("hello")))))
    val exp7 = YourChoice(Negation(expr), List(NoChoice[Textline](Textline("hello"))), None)
    assertResult(exp7)(rewrite(flipNegation)(src7))
  }

  test("flattenLeft"){
    // Here it is harder to write in AST format directly, so I will use files for this and parse them instead.
    val src = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/flattenLeft.txt")).mkString
    val exp = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/flattenLeft-exp.txt")).mkString
    val srcAST = rewrite(transformToList)(Parser.parseString(src).right.get)
    val expAST = rewrite(transformToList)(Parser.parseString(exp).right.get)
    val res = flattenLeft(srcAST,Defined("A"))
    res foreach println
    assertResult(flattenLeft(srcAST,Defined("A")))(expAST)

  }

  test("flattenRight"){
    // Here it is harder to write in AST format directly, so I will use files for this and parse them instead.
    val src = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/flattenRight.txt")).mkString
    val exp = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/flattenRight-exp.txt")).mkString
    val srcAST = rewrite(transformToList)(Parser.parseString(src).right.get)
    val expAST = rewrite(transformToList)(Parser.parseString(exp).right.get)
    val res = flattenRight(srcAST,Defined("A"))
    res foreach println
    assertResult(res)(expAST)

  }

  test("choiceDomination"){
    // Here it is harder to write in AST format directly, so I will use files for this and parse them instead.
    val src = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/flattenRight.txt")).mkString
    val exp = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/flattenRight-exp.txt")).mkString
    val srcAST = rewrite(transformToList)(Parser.parseString(src).right.get)
    val expAST = rewrite(transformToList)(Parser.parseString(exp).right.get)
    val res = rewrite(choiceDomination)(srcAST)
    assertResult(res)(expAST)

    val src3 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/flattenLeft1.txt")).mkString
    val exp3 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/flattenLeft1-exp.txt")).mkString
    val srcAST3 = rewrite(transformToList)(Parser.parseString(src3).right.get)
    val expAST3 = rewrite(transformToList)(Parser.parseString(exp3).right.get)
    val res3 = rewrite(choiceDomination)(srcAST3)
    assertResult(expAST3)(res3)

    val src2 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/flattenLeft.txt")).mkString
    val exp2 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/flattenLeft-exp.txt")).mkString
    val srcAST2 = rewrite(transformToList)(Parser.parseString(src2).right.get)
    val expAST2 = rewrite(transformToList)(Parser.parseString(exp2).right.get)
    val res2 = rewrite(choiceDomination)(srcAST2)
    assertResult(expAST2)(res2)

    val src1 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/complex4.txt")).mkString
    val exp1 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/complex4-exp.txt")).mkString
    val srcAST1 = rewrite(transformToList)(Parser.parseString(src1).right.get)
    val expAST1 = rewrite(transformToList)(Parser.parseString(exp1).right.get)
    val res1 = rewrite(choiceDomination)(srcAST1)
    res1 foreach println
    assertResult(expAST1)(res1)

  }

//  test("choiceDom"){
//    // Here it is harder to write in AST format directly, so I will use files for this and parse them instead.
//    val src = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/flattenRight.txt")).mkString
//    val exp = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/flattenRight-exp.txt")).mkString
//    val srcAST = rewrite(transformToList)(Parser.parseString(src).right.get)
//    val expAST = rewrite(transformToList)(Parser.parseString(exp).right.get)
//    val res = rewrite(choiceDom)(srcAST)
//    //PrettyPrinter.prettyPrint(res) foreach println
//    assertResult(expAST)(res)
//
//    val src2 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/complex4.txt")).mkString
//    val exp2 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/complex4-exp.txt")).mkString
//    val srcAST2 = rewrite(transformToList)(Parser.parseString(src2).right.get)
//    val expAST2 = rewrite(transformToList)(Parser.parseString(exp2).right.get)
//    val res2 = rewrite(choiceDom)(srcAST2)
//    //PrettyPrinter.prettyPrint(res2) foreach println
//    assertResult(expAST2)(res2)
//
//
//    val src3 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/flattenLeft1.txt")).mkString
//    val exp3 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/flattenLeft1-exp.txt")).mkString
//    val srcAST3 = rewrite(transformToList)(Parser.parseString(src3).right.get)
//    val expAST3 = rewrite(transformToList)(Parser.parseString(exp3).right.get)
//    val res3 = rewrite(choiceDom)(srcAST3)
//    assertResult(expAST3)(res3)
//
//    val src1 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/flattenLeft.txt")).mkString
//    val exp1 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/flattenLeft-exp.txt")).mkString
//    val srcAST1 = rewrite(transformToList)(Parser.parseString(src1).right.get)
//    val expAST1 = rewrite(transformToList)(Parser.parseString(exp1).right.get)
//    val res1 = rewrite(choiceDom)(srcAST1)
//    assertResult(expAST1)(res1)
//  }


  test("ChoiceDomA"){
    val src1 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/flattenLeft.txt")).mkString
    val exp1 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/flattenLeft-exp.txt")).mkString
    val srcAST1 = rewrite(transformToList)(Parser.parseString(src1).right.get)
    val expAST1 = rewrite(transformToList)(Parser.parseString(exp1).right.get)
    val res1 = rewrite(choiceDomA)(srcAST1)
    //PrettyPrinter.prettyPrint(res1) foreach println
    assertResult(expAST1)(res1)

    val src2 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/complex4.txt")).mkString
    val exp2 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/complex4-exp.txt")).mkString
    val srcAST2 = rewrite(transformToList)(Parser.parseString(src2).right.get)
    val expAST2 = rewrite(transformToList)(Parser.parseString(exp2).right.get)
    val res2 = (rewrite(choiceDomA)(srcAST2))
    //PrettyPrinter.prettyPrint(rewrite(choiceDomA)(res2)) foreach println
    assertResult(expAST2)(res2)

    val src3 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/complex6.txt")).mkString
    val exp3 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/complex6-exp.txt")).mkString
    val srcAST3 = rewrite(transformToList)(Parser.parseString(src3).right.get)
    val expAST3 = rewrite(transformToList)(Parser.parseString(exp3).right.get)
    val res3 = (rewrite(choiceDomA)(srcAST3))
    //PrettyPrinter.prettyPrint(res3) foreach println
    assertResult(expAST3)(res3)

    val src4 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/flattenLeft1.txt")).mkString
    val exp4 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/flattenLeft1-exp.txt")).mkString
    val srcAST4 = rewrite(transformToList)(Parser.parseString(src4).right.get)
    val expAST4 = rewrite(transformToList)(Parser.parseString(exp4).right.get)
    val res4 = rewrite(choiceDomA)(srcAST4)
    //PrettyPrinter.prettyPrint(res1) foreach println
    assertResult(expAST4)(res4)

    val src5 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/flattenRight.txt")).mkString
    val exp5 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/flattenRight-exp.txt")).mkString
    val srcAST5 = rewrite(transformToList)(Parser.parseString(src5).right.get)
    val expAST5 = rewrite(transformToList)(Parser.parseString(exp5).right.get)
    val res5 = rewrite(choiceDomA)(srcAST5)
    //PrettyPrinter.prettyPrint(res5) foreach println
    assertResult(expAST5)(res5)

    val src6 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/simple.txt")).mkString
    val exp6 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/simple-exp.txt")).mkString
    val srcAST6 = rewrite(transformToList)(Parser.parseString(src6).right.get)
    val expAST6 = rewrite(transformToList)(Parser.parseString(exp6).right.get)
    val res6 = rewrite(choiceDomA)(srcAST6)
    //PrettyPrinter.prettyPrint(res6) foreach println
    assertResult(expAST6)(res6)

    val src7 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/complex1.txt")).mkString
    val exp7 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/complex1-exp.txt")).mkString
    val srcAST7 = rewrite(transformToList)(Parser.parseString(src7).right.get)
    val expAST7 = rewrite(transformToList)(Parser.parseString(exp7).right.get)
    val res7 = rewrite(choiceDomA)(srcAST7)
    //PrettyPrinter.prettyPrint(res7) foreach println
    assertResult(expAST7)(res7)

    val src8 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/complex2.txt")).mkString
    val exp8 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/complex2-exp.txt")).mkString
    val srcAST8 = rewrite(transformToList)(Parser.parseString(src8).right.get)
    val expAST8 = rewrite(transformToList)(Parser.parseString(exp8).right.get)
    val res8 = rewrite(choiceDomA)(srcAST8)
    //PrettyPrinter.prettyPrint(res8) foreach println
    assertResult(expAST8)(res8)

    val src9 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/complex3.txt")).mkString
    val exp9 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/complex3-exp.txt")).mkString
    val srcAST9 = rewrite(transformToList)(Parser.parseString(src9).right.get)
    val expAST9 = rewrite(transformToList)(Parser.parseString(exp9).right.get)
    val res9 = rewrite(choiceDomA)(srcAST9)
    //PrettyPrinter.prettyPrint(res9) foreach println
    assertResult(expAST9)(res9)

    val src10 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/complex5.txt")).mkString
    val exp10 = io.Source.fromURL(getClass.getResource("/rewriter/choicedom/complex5-exp.txt")).mkString
    val srcAST10 = rewrite(transformToList)(Parser.parseString(src10).right.get)
    val expAST10 = rewrite(transformToList)(Parser.parseString(exp10).right.get)
    val res10 = rewrite(choiceDomA)(srcAST10)
    //PrettyPrinter.prettyPrint(res10) foreach println
    assertResult(expAST10)(res10)
  }

  test("factoring") {
    val src = List(YourChoice(Identifier("A"), List(NoChoice[Textline](Textline("hi"))), None),YourChoice(Identifier("A"), List(NoChoice[Textline](Textline("hello"))), None))
    val exp = List(YourChoice(Identifier("A"), List(NoChoice[Textline](Textline("hi")),NoChoice[Textline](Textline("hello"))), None))
    assertResult(exp)(rewrite(factoring)(src))

    val src1 = List(YourChoice(Identifier("A"), List(NoChoice[Textline](Textline("hi"))), Some(List(NoChoice[Textline](Textline("else"))))),YourChoice(Identifier("A"), List(NoChoice[Textline](Textline("hello"))), None))
    val exp1 = List(YourChoice(Identifier("A"), List(NoChoice[Textline](Textline("hi")),NoChoice[Textline](Textline("hello"))), Some(List(NoChoice[Textline](Textline("else"))))))
    assertResult(exp1)(rewrite(factoring)(src1))

    val src2 = List(YourChoice(Negation(Identifier("A")), List(NoChoice[Textline](Textline("hi"))), Some(List(NoChoice[Textline](Textline("else"))))),YourChoice(Identifier("A"), List(NoChoice[Textline](Textline("hello"))), None))
    val exp2 = List(YourChoice(Negation(Identifier("A")), List(NoChoice[Textline](Textline("hi"))), Some(List(NoChoice[Textline](Textline("else")),NoChoice[Textline](Textline("hello"))))))
    assertResult(exp2)(rewrite(factoring)(src2))

  }

  test("astFACTIdemp"){
    val src = io.Source.fromURL(getClass.getResource("/rewriter/astfactidemp.txt")).mkString
    val exp = io.Source.fromURL(getClass.getResource("/rewriter/astfactidemp-exp.txt")).mkString
    val srcAST = rewrite(transformToList)(Parser.parseString(src).right.get)
    val expAST = rewrite(transformToList)(Parser.parseString(exp).right.get)

    val res1 = rewrite(astFACTIdemp)(srcAST)
    val res = rewrite(removeEmptyBranches)(res1)
    //PrettyPrinter.prettyPrint(res) foreach println
    //assertResult(expAST)(res)

    val src1 = List(YourChoice(And(Identifier("CONFIG_X86"),Identifier("CONFIG_ACPI_WMI")),List(NoChoice[Textline](Textline("#include <string.h>")),NoChoice[Textline](Textline(" ")),NoChoice[Textline](Textline(""" static char block_object_id[3] = "XX";""")),NoChoice[Textline](Textline(" ")), NoChoice[Textline](Textline("void wmi_query_block()")),NoChoice[Textline](Textline(" {")),NoChoice[Textline](Textline("   char method[5]; // (4) just four bytes long")),NoChoice[Textline](Textline(" ")),NoChoice[Textline](Textline("""   strcpy(method, "WQ"); // (5) writes two bytes plus '\0'""")),NoChoice[Textline](Textline("   strncat(method, block_object_id, 2); // (6) ERROR: no space for the final '\0'")),NoChoice[Textline](Textline(" }")),NoChoice[Textline](Textline(" ")),NoChoice[Textline](Textline(" int get_wmid_devices(void)")),NoChoice[Textline](Textline(" {")),NoChoice[Textline](Textline("   wmi_query_block(); // (3)")),NoChoice[Textline](Textline("   return 0;")),NoChoice[Textline](Textline(" }")),NoChoice[Textline](Textline(" ")),NoChoice[Textline](Textline(" int acer_wmi_init(void)")),NoChoice[Textline](Textline(" {")),NoChoice[Textline](Textline("   get_wmid_devices(); // (2)")),NoChoice[Textline](Textline("   return 0;")),NoChoice[Textline](Textline(" }")),NoChoice[Textline](Textline(" ")),NoChoice[Textline](Textline(" ")),NoChoice[Textline](Textline(" int main()")),NoChoice[Textline](Textline(" {")),NoChoice[Textline](Textline("   acer_wmi_init(); // (1))"))),Some(List(NoChoice[Textline](Textline(" ")),NoChoice[Textline](Textline(" ")),NoChoice[Textline](Textline(" int main()")),NoChoice[Textline](Textline(" {"))))),NoChoice[Textline](Textline("   return 0;")),NoChoice[Textline](Textline(" }")))
    val src3 = List(YourChoice(And(Identifier("CONFIG_X86"),Identifier("CONFIG_ACPI_WMI")),List(NoChoice[Textline](Textline("#include <string.h>")),NoChoice[Textline](Textline(" ")),NoChoice[Textline](Textline(""" static char block_object_id[3] = "XX";""")),NoChoice[Textline](Textline(" ")), NoChoice[Textline](Textline("void wmi_query_block()")),NoChoice[Textline](Textline(" {")),NoChoice[Textline](Textline("   char method[5]; // (4) just four bytes long")),NoChoice[Textline](Textline(" ")),NoChoice[Textline](Textline("""   strcpy(method, "WQ"); // (5) writes two bytes plus '\0'""")),NoChoice[Textline](Textline("   strncat(method, block_object_id, 2); // (6) ERROR: no space for the final '\0'")),NoChoice[Textline](Textline(" }")),NoChoice[Textline](Textline(" ")),NoChoice[Textline](Textline(" int get_wmid_devices(void)")),NoChoice[Textline](Textline(" {")),NoChoice[Textline](Textline("   wmi_query_block(); // (3)")),NoChoice[Textline](Textline("   return 0;")),NoChoice[Textline](Textline(" }")),NoChoice[Textline](Textline(" ")),NoChoice[Textline](Textline(" int acer_wmi_init(void)")),NoChoice[Textline](Textline(" {")),NoChoice[Textline](Textline("   get_wmid_devices(); // (2)")),NoChoice[Textline](Textline("   return 0;")),NoChoice[Textline](Textline(" }")),NoChoice[Textline](Textline(" ")),NoChoice[Textline](Textline(" ")),NoChoice[Textline](Textline(" int main()")),NoChoice[Textline](Textline(" {")),NoChoice[Textline](Textline("   acer_wmi_init(); // (1))"))),Some(List(NoChoice[Textline](Textline("")),NoChoice[Textline](Textline(" ")),NoChoice[Textline](Textline(" int main()")),NoChoice[Textline](Textline(" {"))))),NoChoice[Textline](Textline("   return 0;")),NoChoice[Textline](Textline(" }")))
    val res2 = rewrite(astFACTIdemp)(src1)
    println(res2)
    PrettyPrinter.prettyPrint(res2) foreach println
    }


  test("Rejoin"){
    val src = List(YourChoice(Identifier("A"), List(NoChoice[Textline](Textline("test"))),None), NoChoice[Textline](Textline(" ")),YourChoice(Identifier("A"), List(NoChoice[Textline](Textline("2nd ifdef"))),None))
    val exp = List(YourChoice(Identifier("A"), List(NoChoice[Textline](Textline("test")), NoChoice[Textline](Textline(" ")),NoChoice[Textline](Textline("2nd ifdef"))),None))

    val res = rewrite(rejoin)(src)
    assertResult(exp)(res)
  }

  test("Nested ifdefs"){
    val src1 = io.Source.fromURL(getClass.getResource("/rewriter/nestedifdefs/test.c")).mkString
    val exp1 = io.Source.fromURL(getClass.getResource("/rewriter/nestedifdefs/test-exp.c")).mkString
    val srcAST1 = rewrite(transformToList)(Parser.parseString(src1).right.get)
    val expAST1 = rewrite(transformToList)(Parser.parseString(exp1).right.get)
//    val res1 = rewrite(nestedIfdefs)(srcAST1)
//    PrettyPrinter.prettyPrint(res1) foreach println

    val src2 = io.Source.fromURL(getClass.getResource("/rewriter/nestedifdefs/planner-clones.cpp")).mkString
//    val exp2 = io.Source.fromURL(getClass.getResource("/rewriter/nestedifdefs/test-exp.c")).mkString
    val srcAST2 = rewrite(transformToList)(Parser.parseString(src2).right.get)
//    val expAST2 = rewrite(transformToList)(Parser.parseString(exp2).right.get)
    val res2 = rewrite(nestedIfdefs)(srcAST2)
    PrettyPrinter.prettyPrint(res2) foreach println
  }


}